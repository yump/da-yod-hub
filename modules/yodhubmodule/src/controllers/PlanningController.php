<?php
/**
 * yodhub module for Craft CMS 3.x
 *
 * Custom module for Dementia Australia - YOD Hub
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2020 Yump
 */

namespace modules\yodhubmodule\controllers;

use modules\yumpmodule\YumpModule;
use modules\yodhubmodule\YodhubModule;

use yii\web\ForbiddenHttpException;

use Craft;
use craft\web\Controller;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your module’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Yump
 * @package   YodhubModule
 * @since     1.0.0
 */
class PlanningController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something',
        // 'test',
        'get-data-for-planning-tool',
        'save-progress',
        'signup',
        'login',
        'pdf-download',
        'pdf-email'
    ];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our module's index action URL,
     * e.g.: actions/yodhub-module/planning
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'Welcome to the DefaultController actionIndex() method';

        return $result;
    }

    /**
     * Handle a request going to our module's actionDoSomething URL,
     * e.g.: actions/yodhub-module/planning/do-something
     *
     * @return mixed
     */
    public function actionDoSomething()
    {
        $result = 'Welcome to the DefaultController actionDoSomething() method';

        return $result;
    }

    /**
     * actions/yodhub-module/planning/get-data-for-planning-tool
     * @return [type] [description]
     */
    public function actionGetDataForPlanningTool()
    {
        return $this->asJson(array(
            'stepDetails' => YodhubModule::$instance->planning->getStepBasicDetails(),
            'questionnaire' => YodhubModule::$instance->planning->getGoalAnalysisQuestionnaire(),
            'plan' => YodhubModule::$instance->planning->getCurrentUserFirstPlanDetails(),
            'generalQuestionSettings' => array(
                '1' => YodhubModule::$instance->planning->getStep1FieldSettings(),
                '2' => YodhubModule::$instance->planning->getStep2EverydayFieldSettings(),
            ),
            // 'isLoggedInUser' => YodhubModule::$instance->planning->validateUserPermission(YumpModule::$instance->yump->getCurrentUser()),
        ));
    }

    /**
     * actions/yodhub-module/planning/save-progress
     * @return [type] [description]
     */
    public function actionSaveProgress()
    {
        // throw new \yii\web\HttpException(500, "You failed!");
        if (YodhubModule::$instance->planning->validateUserPermission(YumpModule::$instance->yump->getCurrentUser())) {
            // Save your progress
            $result = YodhubModule::$instance->planning->savePlanByPostRequest();

            return $this->asJson($result);
        } else {
            throw new ForbiddenHttpException('Requires login and planning tool user permission.');
        }
    }

    public function actionSignup()
    {

    }

    public function actionLogin()
    {

    }

    public function actionTest()
    {
        $this->requireAdmin();

        $result = YodhubModule::$instance->planning->savePlanByPostRequest();

        die();
    }

    /**
     * Generate report pdf file to download
     * This function only generate pdf for download it will NOT save file on server
     * e.g.: actions/yodhub-module/planning/pdf-download
     *
     * @return mixed
     */
    public function actionPdfDownload()
    {
        $this->requirePostRequest();
        $requests = Craft::$app->getRequest()->post();
        $data = [
            'step1' => [],
            'step2' => [],
            'goalsAnalysisAnswers' => [],
            'steps' => [],
            'definitions' => [],
        ];

        if (!empty($requests['general']) && is_array($requests['general'])) {
            //loop step 1 and 2
            if (!empty($requests['general'][1])) {
                $data['step1'] = YodhubModule::$instance->planning->preparePdfStep1($requests['general'][1]);
            }
            if (!empty($requests['general'][2])) {
                $data['step2'] = YodhubModule::$instance->planning->preparePdfStep2($requests['general'][2]);
            }
        }

        if (!empty($requests['goalsAnalysisAnswers'])) {
            $data['goalsAnalysisAnswers'] = YodhubModule::$instance->planning->preparePdfAnalysisAnswers($requests['goalsAnalysisAnswers'], $requests['goalsRanking']);
        }

        if(!empty($requests['optionalGoals'])) {
            $data['optionalGoals'] = $requests['optionalGoals'];
        }

        $data['steps'] = YodhubModule::$instance->planning->preparePdfSteps();
        $data['definitions'] = YodhubModule::$instance->planning->preparePdfDefinition();
        $pdf = YodhubModule::$instance->planning->getPdfReport($data);
        return "";
    }

    /**
     * Generate report pdf file to download
     * This function only generate pdf for download it will NOT save file on server
     * e.g.: actions/yodhub-module/planning/pdf-download
     *
     * @return mixed
     */
    public function actionPdfEmail()
    {
        $this->requirePostRequest();
        $requests = Craft::$app->getRequest()->post();

        $recaptcha = YodhubModule::$instance->planning->checkreCaptcha($requests['shareReCaptcha']);

        if (!$recaptcha) {
            throw new \yii\web\HttpException(400, "You are seen as spam! If you are not, please make sure your progress is saved, refresh the page and try again.");
            return false;
        }

        $sharePdf = json_decode($requests['sharePdf'], true);

        $emailContent = '';
        $ndisPlanningTool = \Craft::$app->getGlobals()->getSetByHandle('ndisPlanningTool');
        if(!empty($ndisPlanningTool->shareNdisPdfEmail)){
            $emailContent .= $ndisPlanningTool->shareNdisPdfEmail->getRawContent();
        }

        $emailContent .= '<p><strong>Email From: </strong>' . $sharePdf['yourEmail'] . '</p>';
        if (!empty($sharePdf['personalMessage'])) {
            $emailContent .= '<p><strong>Message: </strong>' . $sharePdf['personalMessage'] . '</p>';
        }

        if (!empty($requests['general']) && is_array($requests['general'])) {
            //loop step 1 and 2
            if (!empty($requests['general'][1])) {
                $data['step1'] = YodhubModule::$instance->planning->preparePdfStep1($requests['general'][1]);
            }
            if (!empty($requests['general'][2])) {
                $data['step2'] = YodhubModule::$instance->planning->preparePdfStep2($requests['general'][2]);
            }
        }

        if (!empty($requests['goalsAnalysisAnswers'])) {
            $data['goalsAnalysisAnswers'] = YodhubModule::$instance->planning->preparePdfAnalysisAnswers($requests['goalsAnalysisAnswers'], $requests['goalsRanking']);
        }

        if(!empty($requests['optionalGoals'])) {
            $data['optionalGoals'] = $requests['optionalGoals'];
        }

        $data['steps'] = YodhubModule::$instance->planning->preparePdfSteps();
        $data['definitions'] = YodhubModule::$instance->planning->preparePdfDefinition();
        $pdf = YodhubModule::$instance->planning->getPdfReport($data, true);
        //Send email
        foreach ($sharePdf['theirEmail'] as $toEmail) {
            if (filter_var($toEmail, FILTER_VALIDATE_EMAIL)) {
                Craft::$app
                    ->getMailer()
                    ->compose()
                    //->setTo("fei@yump.com.au")
                    ->setTo($toEmail)
                    ->attach($pdf)
                    ->setSubject("NDIS Report")
                    ->setHtmlBody($emailContent)
                    ->send();
            }
        }


        unlink($pdf);
        //return '';
        //return "<script>window.close();</script>";
        return $this->redirect('/thank-you/your-plan-has-been-send');
    }
}
