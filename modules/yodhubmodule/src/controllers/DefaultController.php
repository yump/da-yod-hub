<?php
/**
 * yodhub module for Craft CMS 3.x
 *
 * Custom module for Dementia Australia - YOD Hub
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2020 Yump
 */

namespace modules\yodhubmodule\controllers;

use modules\yumpmodule\YumpModule;
use modules\yodhubmodule\YodhubModule;

use Craft;
use craft\web\Controller;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your module’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Yump
 * @package   YodhubModule
 * @since     1.0.0
 */
class DefaultController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something',
        // 'test',
    ];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our module's index action URL,
     * e.g.: actions/yodhub-module/default
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'Welcome to the DefaultController actionIndex() method';

        return $result;
    }

    /**
     * Handle a request going to our module's actionDoSomething URL,
     * e.g.: actions/yodhub-module/default/do-something
     *
     * @return mixed
     */
    public function actionDoSomething()
    {
        $result = 'Welcome to the DefaultController actionDoSomething() method';

        return $result;
    }

    public function actionTest() {
        $this->requireAdmin();

        print_r(YodhubModule::$instance->planning->getOptionalGoalsVueReady());
        die();

        $planId = 6972;
        $userId = 1;

        $currentUser = YumpModule::$instance->yump->getCurrentUser();
        // $plan = YumpModule::$instance->yump->getEntryByIdFix(6972);
        // $plans = YodhubModule::$instance->planning->getCurrentUserPlans();
        // $currentUserFirstPlan = YodhubModule::$instance->planning->getCurrentUserFirstPlan();
        $currentUserFirstPlanDetails = YodhubModule::$instance->planning->getCurrentUserFirstPlanDetails();

        // $answers = YodhubModule::$instance->planning->getGoalAnalysisAnswers($planId);
        // $ranks = YodhubModule::$instance->planning->getGoalAnalysisRanks($planId);
        // $generalQuestionAnswers = YodhubModule::$instance->planning->getGeneralQuestionAnswers($plan);
        // $planDetails = YodhubModule::$instance->planning->getPlanDetails($planId);
        
        // $steps = YodhubModule::$instance->planning->getStepBasicDetails();
        
        // $topic = craft\elements\Category::find()->id(6679)->one();
        // $questions = YodhubModule::$instance->planning->getTopicRelatedQuestions($topic);
        
        $questionnaire = YodhubModule::$instance->planning->getGoalAnalysisQuestionnaire();

        YumpModule::$instance->yump->dump($currentUserFirstPlanDetails);

        die();
    }
}
