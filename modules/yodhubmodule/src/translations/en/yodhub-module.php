<?php
/**
 * yodhub module for Craft CMS 3.x
 *
 * Custom module for Dementia Australia - YOD Hub
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2020 Yump
 */

/**
 * yodhub en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('yodhub-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Yump
 * @package   YodhubModule
 * @since     1.0.0
 */
return [
    'yodhub plugin loaded' => 'yodhub plugin loaded',
];
