/**
 * yodhub module for Craft CMS
 *
 * YodhubModuleUtility Utility JS
 *
 * @author    Yump
 * @copyright Copyright (c) 2020 Yump
 * @link      https://yump.com.au
 * @package   YodhubModule
 * @since     1.0.0
 */
