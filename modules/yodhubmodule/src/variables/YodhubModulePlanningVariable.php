<?php
/**
 * yodhub module for Craft CMS 3.x
 *
 * Custom module for Dementia Australia - YOD Hub
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2020 Yump
 */

namespace modules\yodhubmodule\variables;

use modules\yodhubmodule\YodhubModule;

use Craft;

/**
 * yodhub Variable
 *
 * Craft allows modules to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.yodhubModule }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Yump
 * @package   YodhubModule
 * @since     1.0.0
 */
class YodhubModulePlanningVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Try redirect to service if needed
     *
     * @param string $method
     * @param array $arguments
     * @return Service function if available
     */
    function __call($method, $arguments) {
        return call_user_func_array(array(YodhubModule::$instance->planning, $method), $arguments);
    }

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.yodhubModule.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.yodhubModule.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function exampleVariable($optional = null)
    {
        $result = "And away we go to the Twig template...";
        if ($optional) {
            $result = "I'm feeling optional today...";
        }
        return $result;
    }
}
