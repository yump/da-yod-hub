<?php
/**
 * yodhub module for Craft CMS 3.x
 *
 * Custom module for Dementia Australia - YOD Hub
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2020 Yump
 */

namespace modules\yodhubmodule\services;

use modules\yumpmodule\YumpModule;

use modules\yodhubmodule\YodhubModule;
use modules\yodhubmodule\helpers\planning\Constants;

use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Mpdf;
use yii\web\HttpException;

use Craft;
use craft\base\Component;
use craft\elements\Entry;

/**
 * YodhubModuleService Service
 *
 * All of your module’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other modules can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Yump
 * @package   YodhubModule
 * @since     1.0.0
 */
class YodhubModulePlanningService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin/module file, call it like this:
     *
     *     YodhubModule::$instance->yodhub->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }

    // ------------------------------------------------------------------ //
    // ------------------ Getting user's plan details --------------------//

    /**
     * Get all the goal analysis answers, given the plan
     * @param  [\craft\elements\Entry or int] $plan either the entry element object, or the element ID of the entry
     * @param  [bool] $toArray whether the results should be converted into array of key:value format (questionId:score), or leave it as array of entries. By default, it will return as key:value format.
     * @return [type]       [description]
     */
    public function getGoalAnalysisAnswers($plan, $toArray = true)
    {
        $answers = craft\elements\Entry::find()
            ->section('goalAnalysisAnswers')
            ->relatedTo([
                'targetElement' => $plan,
                'field' => 'ndisPlan',
            ])
            ->with(['goalAnalysisQuestion'])
            ->limit(null)
            ->all();

        $scoreRanges = [
            'needs' => $this->getScoreRange('needs'),
            'personalImportance' => $this->getScoreRange('personalImportance'),
        ];

        if ($toArray) {
            $answersArr = [];
            foreach ($answers as $answer) {
                $question = $answer->goalAnalysisQuestion[0] ?? null;
                if ($question) {
                    $score = $answer->score;

                    /**
                     * This chunk of code is to adapt the change that certain score definitions are disabled as per user testing outcome. It then causes some score existing in the db to be out of the score range (the range is based on the enabled score definitions). Rather than updating them in db, we put in this chunk of code to force it to be the max / min of the range if it's out of range.
                     * @var [type]
                     */
                    $goalAnalysisType = $question->goalAnalysisType->value;
                    if(!empty($scoreRanges[$goalAnalysisType])) {
                        if($score > $scoreRanges[$goalAnalysisType]['max']) {
                            $score = (int)$scoreRanges[$goalAnalysisType]['max'];
                        } else if ($score < $scoreRanges[$goalAnalysisType]['min']) {
                            $score = (int)$scoreRanges[$goalAnalysisType]['min'];
                        }
                    }
                    // //////////////////
                    
                    $answersArr[$question->id] = $score;
                }
            }
            return $answersArr;
        } else {
            return $answers;
        }
    }

    public function getScoreRange($goalAnalysisType) {
        return array(
            'max' => $this->getMaxScorePerType($goalAnalysisType),
            'min' => $this->getMinScorePerType($goalAnalysisType),
        );
    }

    /**
     * The score definiation options have been changed. Initially we have 5, but now cut down to 3. To avoid updating db to change those existing records with score outside the current range stored in database, we detect the max and min possible score for a goalAnalysisType (needs / personalImportance) and adjust the data returned to the front end accordingly.
     * @param  [string] $goalAnalysisType [description]
     * @return [type]       [description]
     */
    public function getMaxScorePerType($goalAnalysisType) {
        $result = \craft\elements\Category::find()
            ->group('scoreDefinitions')
            ->where(["field_goalAnalysisType" => $goalAnalysisType])
            ->max("field_score");
        return $result;
    }

    /**
     * Same comment as above
     * @param  [type] $goalAnalysisType [description]
     * @return [type]                   [description]
     */
    public function getMinScorePerType($goalAnalysisType) {
        $result = \craft\elements\Category::find()
            ->group('scoreDefinitions')
            ->where(["field_goalAnalysisType" => $goalAnalysisType])
            ->min("field_score");
        return $result;
    }

    /**
     * Get all the goal analysis ranks, given the plan
     * @param  [\craft\elements\Entry or int] $plan either the entry element object, or the element ID of the entry
     * @param  [bool] $toArray whether the results should be converted into array of key:value format (goalId:rank), or leave it as array of entries. By default, it will return as key:value format.
     * @return [type]       [description]
     */
    public function getGoalAnalysisRanks($plan, $toArray = true)
    {
        $ranks = craft\elements\Entry::find()
            ->section('goalAnalysisRanks')
            ->relatedTo([
                'targetElement' => $plan,
                'field' => 'ndisPlan',
            ])
            ->with(['goal'])
            ->limit(null)
            ->all();
        if ($toArray) {
            $ranksArr = [];
            foreach ($ranks as $rankEntry) {
                $goal = $rankEntry->goal[0] ?? null;
                if ($goal) {
                    $ranksArr[$goal->id] = array(
                        'rank' => $rankEntry->rank,
                        'notes' => $rankEntry->notes,
                    );
                }
            }
            return $ranksArr;
        } else {
            return $ranks;
        }
    }

    /**
     * Get all the answers for the general questions of a plan
     * @param  [\craft\elements\Entry or int] $plan either the entry element object, or the element ID of the entry
     * @return [type]       [description]
     */
    public function getGeneralQuestionAnswers($plan)
    {
        $fieldHandles = Constants::GENERAL_QUESTIONS_FIELD_HANDLES;
        $answers = [];
        foreach ($fieldHandles as $fieldHandle) {
            $answers[$fieldHandle] = @$plan[$fieldHandle];
        }
        return $answers;
    }

    /**
     * Get all the optional goals of this plan
     * @param  [\craft\elements\Entry or int] $plan either the entry element object, or the element ID of the entry
     * @return [type]       [description]
     */
    public function getOptionalGoals($plan, $toArray = true) {
        $result = \craft\elements\Entry::find()
            ->section('optionalGoals')
            ->relatedTo([
                'targetElement' => $plan,
                'field' => 'ndisPlan',
            ])
            ->with(['goalAnalysisTopic'])
            ->limit(null)
            ->all();
        if ($toArray) {
            $resultArr = [];
            foreach ($result as $optionalGoalEntry) {
                $goalAnalysisTopic = $optionalGoalEntry->goalAnalysisTopic[0] ?? null;
                if ($goalAnalysisTopic) {
                    $resultArr[$goalAnalysisTopic->id] = $optionalGoalEntry->optionalGoal;
                }
            }
            return $resultArr;
        } else {
            return $result;
        }
    }

    /**
     * Get the saved plan details (meaning all the user submitted data of a plan)
     * @param  [\craft\elements\Entry or int] $plan either the entry element object, or the element ID of the entry
     * @return array  array of the plan details in different keys
     */
    public function getPlanDetails($plan)
    {
        if (!($plan instanceof \craft\elements\Entry)) {
            $plan = YumpModule::$instance->yump->getEntryByIdFix($plan);
        }

        return array(
            'currentStep' => $plan->currentStep,
            'goalAnalysisAnswers' => YodhubModule::$instance->planning->getGoalAnalysisAnswers($plan),
            'goalAnalysisRanks' => YodhubModule::$instance->planning->getGoalAnalysisRanks($plan),
            'generalQuestionAnswers' => YodhubModule::$instance->planning->getGeneralQuestionAnswers($plan),
            'optionalGoals' => YodhubModule::$instance->planning->getOptionalGoals($plan),
        );
    }

    public function validateUserPermission($user)
    {
        return $user and $user->isInGroup('planningToolUsers');
    }

    /**
     * Get the list of plan entries of a given user
     * @param  [\craft\elements\User or int] $user either the user element object, or the element ID of the user
     * @return [array|null]       array of plan entries, or null if user does not have permission (not in planningToolUsers group).
     */
    public function getUserPlans($user)
    {
        if ($this->validateUserPermission($user)) {
            return craft\elements\Entry::find()
                ->section('plans')
                ->relatedTo([
                    'targetElement' => $user,
                    'field' => 'planningToolUser',
                ])
                ->limit(null)
                ->all();
        }
    }

    /**
     * Get current user's list of plan entries
     * @return array|null (null if no current user)
     */
    public function getCurrentUserPlans()
    {
        $currentUser = YumpModule::$instance->yump->getCurrentUser();
        if ($currentUser) {
            return $this->getUserPlans($currentUser);
        }
    }

    /**
     * Get current user's first plan entry
     * @return \craft\element\Entry|null
     */
    public function getCurrentUserFirstPlan()
    {
        $currentUserPlans = $this->getCurrentUserPlans();
        return $currentUserPlans[0] ?? null;
    }

    /**
     * Get current user's first plan details using the getPlanDetails() function in this class
     * @return array|null  see $this->getPlanDetails()
     */
    public function getCurrentUserFirstPlanDetails()
    {
        $currentUserFirstPlan = $this->getCurrentUserFirstPlan();
        if ($currentUserFirstPlan) {
            return $this->getPlanDetails($currentUserFirstPlan);
        }
    }

    // ----------- End of getting user's plan details --------------------//
    // ------------------------------------------------------------------ //


    // ----- Getting the details of planning tool steps to help rendering the questionnaire on the front end. ----------------------------------//
    // -------------------------------------------------------------------//

    /**
     * May not be in use
     *
     * get the list of goal analysis topics
     * @param boolean $toArray whether the results should be converted into array of key:value format (id:title), or leave it as array of category objects. By default, it will return as key:value format.
     * @return [type]           [description]
     */
    public function getGoalAnalysisTopics($toArray = true)
    {
        $topics = craft\elements\Category::find()
            ->group('goalAnalysisTopics')
            ->limit(null)
            ->all();
        if ($toArray) {
            $topicsArr = [];
            foreach ($topics as $topic) {
                $topicsArr[$topic->id] = $topic->title;
            }
            return $topicsArr;
        } else {
            return $topics;
        }
    }

    /**
     * Get each step's title, featured image, intro section, step ID, topic ID, default summary, banner summary and result summary.
     *
     * The main usage is to display the basic information for each step on the front end: 1. on Your plan page (via Twig), 2. on questionnaire page (via Ajax and Vue), 3. on results page (via Ajax and Vue), 4. on the print PDF (via PHP)
     * @return array  list of steps with its details (in stepId:array format)
     */
    public function getStepBasicDetails()
    {
        $steps = craft\elements\Entry::find()
            ->section('steps')
            ->with(['goalAnalysisTopic'])
            ->limit(null)
            ->orderBy('stepId')
            ->all();

        $stepsArr = [];
        foreach ($steps as $step) {
            $defaultSummary = $step->summary ? $step->summary->getParsedContent() : null;
            $bannerSummary = $step->bannerSummary ? $step->bannerSummary->getParsedContent() : $defaultSummary;
            $resultSummary = $step->resultSummary ? $step->resultSummary->getParsedContent() : $defaultSummary;
            $intro = $step->intro ? $step->intro->getParsedContent() : null;
            $featuredImageObj = $step->featuredImage ? $step->featuredImage->one() : null;
            $planningStepImagerSettings = YumpModule::$instance->yump->getConfig('planningStepImagerSettings');
            $imageUrl = $featuredImageObj ? YumpModule::$instance->yump->getThumbnail($featuredImageObj, $planningStepImagerSettings) : null;
            $imageTitle = $featuredImageObj->title;
            $imageFocalPoint = $featuredImageObj ? $featuredImageObj->getFocalPoint(true) : null;
            $topic = $step->goalAnalysisTopic[0] ?? null;
            $stepsArr[] = array(
                'id' => $step->id,
                'stepId' => $step->stepId,
                'title' => $step->title,
                'defaultSummary' => $defaultSummary,
                'bannerSummary' => $bannerSummary,
                'resultSummary' => $resultSummary,
                'intro' => $intro,
                'imageUrl' => $imageUrl,
                'imageTitle' => $imageTitle ,
                'imageFocalPoint' => $imageFocalPoint,
                'topicId' => $topic ? $topic->id : null,
            );
        }

        return $stepsArr;
    }

    /**
     * Gather all necessary information and convert it into a format which is handy for front end / PDF to generate the form fields (for goal analysis questions only).
     * @return [type] [description]
     */
    public function getGoalAnalysisQuestionnaire()
    {
        $steps = craft\elements\Entry::find()
            ->section('steps')
            ->with(['goalAnalysisTopic'])
            ->limit(null)
            ->orderBy('stepId')
            ->all();

        $result = [
            'steps' => [],
            'scoreDefinitions' => [
                'needs' => $this->getScoreDefinitions('needs'),
                'personalImportance' => $this->getScoreDefinitions('personalImportance'),
            ],
        ];

        // get necessary data for each step's questionnaire (for goals analysis steps only)
        foreach ($steps as $step) {
            $topic = $step->goalAnalysisTopic[0] ?? null;
            // a step without a goal analysis topic is not related to goal analysis, which is not considered in this function.
            if ($topic) {
                $stepObj = [
                    'id' => $step->id,
                    'stepId' => $step->stepId,
                    'topic' => [
                        'id' => $topic->id,
                        'title' => $topic->title,
                    ],
                ];

                $questionsAndGoals = $this->getTopicRelatedQuestionsAndGoals($topic);
                $stepObj['topic']['goals'] = $questionsAndGoals['goals'];
                $stepObj['topic']['questions'] = $questionsAndGoals['questions'];
                $result['steps'][] = $stepObj;
            }
        }

        return $result;
    }

    public function getScoreDefinitions($goalAnalysisType)
    {
        $orderByDirection = "asc";
        if ($goalAnalysisType == 'personalImportance') {
            $orderByDirection = "desc";
        }
        $definitions = craft\elements\Category::find()
            ->group('scoreDefinitions')
            ->orderBy("score " . $orderByDirection)
            ->goalAnalysisType($goalAnalysisType)
            ->all();

        $result = [];
        foreach ($definitions as $definition) {
            $result[] = [
                'id' => $definition->id,
                'title' => $definition->title,
                'score' => $definition->score,
                'type' => $goalAnalysisType,
            ];
        }

        return $result;
    }

    public function getTopicRelatedQuestionsAndGoals($topic)
    {
        $goals = craft\elements\Category::find()
            ->group('goals')
            ->relatedTo([
                'targetElement' => $topic,
                'field' => 'goalAnalysisTopic',
            ])
            ->limit(null)
            ->all();

        $goalsArr = [];

        $relatedGoalsParams = ['or'];
        foreach ($goals as $goal) {
            $goalsArr[] = [
                'id' => $goal->id,
                'title' => $goal->title,
            ];

            // constructing the relatedTo param for the later questions query
            $relatedGoalsParams[] = [
                'targetElement' => $goal,
                'field' => 'goal',
            ];
        }

        $questions = [
            'needs' => $this->_getQuestionsByType('needs', $relatedGoalsParams),
            'personalImportance' => $this->_getQuestionsByType('personalImportance', $relatedGoalsParams),
        ];

        return [
            'goals' => $goalsArr,
            'questions' => $questions,
        ];
    }

    /**
     * private function made for the sake of DRY. Get list of questions in front end friendly array, given the $goalAnalysisType, and the pre-constructed $relatedGoalsParams
     * @param  [string] $goalAnalysisType               [description]
     * @param  [array] $relatedGoalsParams [description]
     * @return [type]                     [description]
     */
    private function _getQuestionsByType($goalAnalysisType, $relatedGoalsParams)
    {
        $questions = craft\elements\Category::find()
            ->group('goalAnalysisQuestions')
            ->relatedTo($relatedGoalsParams)
            ->goalAnalysisType($goalAnalysisType)
            ->with(['goal'])
            ->limit(null)
            ->all();

        $result = [];
        foreach ($questions as $question) {
            $result[] = [
                'id' => $question->id,
                'title' => $question->title,
                'goalId' => $question->goal[0]->id, // assuming every question will definitely have a goal (otherwise this will cause error)
                'type' => $goalAnalysisType,
            ];
        }

        return $result;
    }

    /**
     * Prepare the general question fields to be front end Vue ready. It will be used as the structure of the general questions part of the formInputs attribute of vue, so we can bind the general question fields with formInputs using v-model, without interrupting the original 'info' attribute.
     * @return [type] [description]
     */
    public function getGeneralQuestionFieldsVueReady()
    {
        $result = [];
        foreach (Constants::GENERAL_QUESTIONS_FIELD_HANDLES as $fieldHandle) {
            $result[$fieldHandle] = null;
        }
        return $result;
    }

    public function getGoalAnalysisQuestionFieldsVueReady()
    {
        $result = [
            'answers' => [],
            'goals' => [],
        ];

        $steps = @$this->getGoalAnalysisQuestionnaire()['steps'];
        if ($steps) {
            foreach ($steps as $step) {
                // prepare for all the questions
                $questions = $step['topic']['questions'];
                foreach ($questions as $questionGroup) {
                    foreach ($questionGroup as $question) {
                        $result['answers'][$question['id']] = null;
                    }
                }

                // prepare for the goals
                $goals = $step['topic']['goals'];
                foreach ($goals as $goal) {
                    $result['goals'][$goal['id']] = [
                        'rank' => null,
                        'notes' => null,
                    ];
                }
            }

            return $result;
        } else {
            throw new HttpException(500, "Cannot retrieve the questionnaire. Please refresh your browser and try again.");
        }

    }

    public function getOptionalGoalsVueReady() {
        $result = [];

        $goalAnalysisTopics = \craft\elements\Category::find()
            ->group("goalAnalysisTopics")
            ->ids();

        foreach ($goalAnalysisTopics as $topicId) {
            $result[$topicId] = "";
        }
        return $result;
    }

    /**
     * To help general step 1 form on the front end via Twig
     * @return [type] [description]
     */
    public function getStep1FieldSettings()
    {
        return Constants::STEP1_FIELD_SETTINGS;
    }

    /**
     * To help general step 2 form on the front end via Twig. This only includes fields related to everyday activities. The last 'Occasional Activities' will be hard coded in Twig manually.
     * @return [type] [description]
     */
    public function getStep2EverydayFieldSettings()
    {
        return Constants::STEP2_EVERYDAY_FIELD_SETTINGS;
    }

    // ================== Plan saving =========================//

    /**
     * Save the plan by the submitted form data from POST request.
     * @return [type] [description]
     */
    public function savePlanByPostRequest()
    {
        $params = Craft::$app->request->getBodyParams();
        //YumpModule::$instance->yump->dump($params);
        $furthestStep = @$params['furthestViewableStep'];
        $currentStep = @$params['currentStep'];
        $stepsNeedSave = @$params['stepsNeedSave'];
        
        if(!isset($params['stepsNeedSave']))
        {
            if($currentStep=='completed')
            {
                $currentStep = 9;
            }
            $stepsNeedSave = array();
            for($i=1; $i<=$currentStep; $i++)
            {
                $stepsNeedSave[] = $i;
            }
            $params['stepsNeedSave'] = $stepsNeedSave;
        }
        
        if (!$furthestStep or !$currentStep) {
            throw new HttpException(500, "Cannot update your profile on the server. Please try again. If the issue persists, please contact us.");
        }

        $plan = $this->getCurrentUserFirstPlan();
        if ($plan) {

            $return = array(
                'success' => false,
                'errors' => [],
            );

            // $return['test'] = [
            //     'params' => $params,
            // ];

            // return $return;

            if (!empty($stepsNeedSave)) {
                if (in_array(1, $stepsNeedSave) or in_array(2, $stepsNeedSave)) {
                    $this->_prepareGeneralAnswers($plan, $params);
                    $planSavedSuccess = Craft::$app->elements->saveElement($plan);
                    if (!$planSavedSuccess) {
                        $return['errors'] = $plan->getErrors();
                        return $return;
                    }
                }

                // $goalAnalysisQuestionnaire = $this->getGoalAnalysisQuestionnaire();

                $answersSavedSuccess = $this->_saveGoalsAnalysisAnswersByPost($plan, $params);
                $ranksSavedSuccess = $this->_saveGoalsAnalysisRanksByPost($plan, $params);
                $optionalGoalsSavedSuccess = $this->_saveOptionalGoalsByPost($plan, $params);
                if (!$answersSavedSuccess or !$ranksSavedSuccess or !$optionalGoalsSavedSuccess) {
                    // $return['errors'][] = array(
                    //     'goalsAnalysis' => 'Invalid answers in your goal analysis forms',
                    // ); // normally saving answers and ranks won't cause issues, and it's too much efforts to catch which ones are having issues. We just throw an exception for the front end to handle.

                    throw new HttpException(500, "Cannot save your progress. Please try again. If the issue persists, please contact us.");
                }

                $continueAndSave = @$params['continue'];
                if ($continueAndSave) {
                    // TODO: update this function to be _maybeUpdateCurrentStep, and only update it when necessary.
                    $furthestStep = $this->getNextStep($plan, $furthestStep);
                }

                if($furthestStep) {
                    $plan->setFieldValue('currentStep', $furthestStep);
                    $success = Craft::$app->elements->saveElement($plan);
                } else {
                    $success = false;
                    throw new HttpException(500, "Your progress is saved, but we could not update your progress status. Please manually refresh the page and try again. If the issue persists, please contact us.");
                }
                
                if ($success) {
                    $return['success'] = true;
                    $returnPlan = @$params['returnPlan'];
                    if($returnPlan) {
                        $return['plan'] = YodhubModule::$instance->planning->getCurrentUserFirstPlanDetails();
                    }
                    
                } else {
                    // $return['errors'] = array_merge($return['errors'], $plan->getErrors());
                    $return['errors'] = $plan->getErrors();
                }
                // return array(
                //     'success' => true,
                //     // 'plan' => YodhubModule::$instance->planning->getCurrentUserFirstPlanDetails(),
                // );
            } else {
                // if no step needs to be saved, simply return success
                $return['success'] = true;
            }

            return $return;
        } else {
            $currentUser = YumpModule::$instance->yump->getCurrentUser();
            $currentUserId = $currentUser ? $currentUser->id : "N/A";
            Craft::error("Cannot find the plan entry for user [ID: " . $currentUserId . "]!", __METHOD__);
            throw new HttpException(500, "Cannot find your profile on the server. Please try again. If the issue persists, please contact us.");
        }
    }

    
    public function getNextStep($plan, $furthestStep) {
        $currentStepInDb = $plan->currentStep;
        if($currentStepInDb != 'completed' and $furthestStep != 'completed') {
            if($furthestStep < $currentStepInDb) {
                return $currentStepInDb;
            } else {
                $stepIds = $this->getAvailableStepIds();
                if(in_array($furthestStep, $stepIds)) {
                    if(in_array($furthestStep + 1, $stepIds)) {
                        return $furthestStep + 1;
                    } else {
                        return 'completed';
                    }
                }
            }
        } else {
            return 'completed';
        }

        return false;
    }

    public function getAvailableStepIds() {
        $steps = craft\elements\Entry::find()
            ->section('steps')
            ->limit(null)
            ->orderBy('stepId')
            ->all();

        $stepIds = [];
        foreach ($steps as $stepEntry) {
            $stepIds[] = $stepEntry->stepId;
        }
        return $stepIds;
    }

    /**
     * Populate the general answers data into the plan entry (before it's saved).
     *
     * Only do that for the steps which need to be saved according to the "stepsNeedSave" param.
     * @param  [type] $plan   [description]
     * @param  [type] $params [description]
     * @return bool|null      true if successful and also data populated. false if no data needs to be populated (nothing needs to be saved for general questions).
     */
    private function _prepareGeneralAnswers($plan, $params)
    {
        $generalAnswers = @$params['general'];
        $stepsNeedSave = @$params['stepsNeedSave'];
        if ($generalAnswers and !empty($stepsNeedSave)) {
            $answerParams = [];

            foreach ($generalAnswers as $stepId => $answers) {
                if (in_array($stepId, $stepsNeedSave)) {
                    $answerParams = array_merge($answerParams, $answers);
                }
            }
            if (!empty($answerParams)) {
                $plan->setFieldValues($answerParams);
                return true;
            }
        }
    }

    /**
     * Save the goals analysis answers from the post
     * @param  [type] $plan   [description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    private function _saveGoalsAnalysisAnswersByPost($plan, $params)
    {
        if (!empty($params['goalsAnalysisAnswers'])) {
            $stepsNeedSave = @$params['stepsNeedSave'];
            $existingGoalAnalysisAnswers = $this->getGoalAnalysisAnswers($plan, false);
            $currentUserId = YumpModule::$instance->yump->getCurrentUser()->id;
            $section = Craft::$app->sections->getSectionByHandle('goalAnalysisAnswers');
            $sectionId = $section->id;
            $typeId = $section->getEntryTypes()[0]->id;
            $planId = $plan->id;

            foreach ($params['goalsAnalysisAnswers'] as $stepId => $answers) {
                if (in_array($stepId, $stepsNeedSave)) {
                    foreach ($answers as $questionId => $score) {
                        // if score is empty, then we don't need to update it or create an entry for it.
                        if ($score) {
                            if ($answer = $this->_findAnswerByQuestion($existingGoalAnalysisAnswers, $questionId)) {
                                // this answer entry already exist
                                $answer->setFieldValues(array('score' => $score));
                            } else {
                                // this answer entry does not exist, we need to create one.
                                $fields = array(
                                    'ndisPlan' => [$planId],
                                    'goalAnalysisQuestion' => [$questionId],
                                    'score' => $score,
                                );
                                $answer = $this->_prepareNewEntry($sectionId, $typeId, $currentUserId, $fields);
                            }

                            $success = Craft::$app->elements->saveElement($answer);
                            if (!$success) {
                                Craft::error("Cannot save the answer entry for user [ID: " . $currentUserId . "]. Plan ID: [" . $planId . "]! Errors:\n" . print_r($answer->getErrors(), true), __METHOD__);
                                return false;
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * This is actually 'find answer by question and plan'. But since the answers array we passed in is already filtered by a given plan, so we don't need to pass in $plan here
     * @param  [type] $answers [description]
     * @return [type]          [description]
     */
    private function _findAnswerByQuestion($answers, $questionId)
    {
        foreach ($answers as $answer) {
            if ($answer['goalAnalysisQuestion'][0]['id'] == $questionId) {
                return $answer;
            }
        }
        return false;
    }

    private function _saveGoalsAnalysisRanksByPost($plan, $params)
    {
        if (!empty($params['goalsRanking'])) {
            $stepsNeedSave = @$params['stepsNeedSave'];
            $existingGoalRanks = $this->getGoalAnalysisRanks($plan, false);
            $currentUserId = YumpModule::$instance->yump->getCurrentUser()->id;
            $section = Craft::$app->sections->getSectionByHandle('goalAnalysisRanks');
            $sectionId = $section->id;
            $typeId = $section->getEntryTypes()[0]->id;
            $planId = $plan->id;

            foreach ($params['goalsRanking'] as $stepId => $rankObjs) {
                if (in_array($stepId, $stepsNeedSave)) {
                    foreach ($rankObjs as $goalId => $rankObj) {
                        // if rankObj is empty or rank is empty, then we don't need to update it or create an entry for it.
                        if ($rankObj and !empty($rankObj['rank'])) {
                            if ($rankEntry = $this->_findRankEntryByGoal($existingGoalRanks, $goalId)) {
                                // this answer entry already exist
                                $rankEntry->setFieldValues(array(
                                    'rank' => $rankObj['rank'],
                                    'notes' => $rankObj['notes'],
                                ));
                            } else {
                                // this answer entry does not exist, we need to create one.
                                $fields = array(
                                    'ndisPlan' => [$planId],
                                    'goal' => [$goalId],
                                    'rank' => $rankObj['rank'],
                                    'notes' => $rankObj['notes'],
                                );
                                $rankEntry = $this->_prepareNewEntry($sectionId, $typeId, $currentUserId, $fields);
                            }

                            $success = Craft::$app->elements->saveElement($rankEntry);
                            if (!$success) {
                                Craft::error("Cannot save the rank entry for user [ID: " . $currentUserId . "]. Plan ID: [" . $planId . "]! Errors:\n" . print_r($rankEntry->getErrors(), true), __METHOD__);
                                return false;
                            }
                        }
                    }
                }
            }

        }

        return true;
    }

    private function _findRankEntryByGoal($rankEntries, $goalId)
    {
        foreach ($rankEntries as $rankEntry) {
            if ($rankEntry['goal'][0]['id'] == $goalId) {
                return $rankEntry;
            }
        }
        return false;
    }

    private function _saveOptionalGoalsByPost($plan, $params) {
        if (!empty($params['optionalGoal'])) {
            // Craft::info(print_r($params['optionalGoal'], true), __METHOD__);

            $stepsNeedSave = @$params['stepsNeedSave'];
            $existingOptionalGoals = $this->getOptionalGoals($plan, false);
            $currentUserId = YumpModule::$instance->yump->getCurrentUser()->id;
            $section = Craft::$app->sections->getSectionByHandle('optionalGoals');
            $sectionId = $section->id;
            $typeId = $section->getEntryTypes()[0]->id;
            $planId = $plan->id;

            foreach ($params['optionalGoal'] as $stepId => $optionalGoals) {
                if (in_array($stepId, $stepsNeedSave)) {
                    foreach ($optionalGoals as $topicId => $optionalGoal) {
                        if ($optionalGoalEntry = $this->_findOptionalGoalByTopic($existingOptionalGoals, $topicId)) {
                            // this answer entry already exist
                            $optionalGoalEntry->setFieldValues(array('optionalGoal' => $optionalGoal));
                        } else {
                            // this answer entry does not exist, we need to create one.
                            $fields = array(
                                'ndisPlan' => [$planId],
                                'goalAnalysisTopic' => [$topicId],
                                'optionalGoal' => $optionalGoal,
                            );
                            $optionalGoalEntry = $this->_prepareNewEntry($sectionId, $typeId, $currentUserId, $fields);
                        }

                        $success = Craft::$app->elements->saveElement($optionalGoalEntry);
                        if (!$success) {
                            Craft::error("Cannot save the optionalGoalEntry entry for user [ID: " . $currentUserId . "]. Plan ID: [" . $planId . "]! Errors:\n" . print_r($optionalGoalEntry->getErrors(), true), __METHOD__);
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * This is actually 'find optionalGoals by topic and plan'. But since the optionalGoals array we passed in is already filtered by a given plan, so we don't need to pass in $plan here
     * @param  [type] $optionalGoals [description]
     * @return [type]          [description]
     */
    private function _findOptionalGoalByTopic($optionalGoals, $topicId)
    {
        foreach ($optionalGoals as $optionalGoal) {
            if ($optionalGoal['goalAnalysisTopic'][0]['id'] == $topicId) {
                return $optionalGoal;
            }
        }
        return false;
    }

    /**
     * Create a new Entry model for the given section and fields. Prepare the model for saving.
     *
     * Please don't reuse it everywhere. Creating a new entry has lots of things invovled. We only simply it based on this particular case (to save new plan, answer and rank). For Craft's saveEntry code, see "EntriesController.php->actionSaveEntry". We do not check for permissions of user for things like "createEntries", "editEntries", "publishEntries", since we assume we have already checked user is in the planningToolUsers group and we assume that we have set adequate permission in that group to do all those things.
     *
     * We also pass in $userId and $typeId, which could be calculated on the fly. But we pre-caculate them to avoid calculating them in the loop.
     * @param  [type] $sectionId [description]
     * @return [type]            [description]
     */
    private function _prepareNewEntry($sectionId, $typeId, $userId, $fields)
    {
        $entry = new Entry();
        $entry->sectionId = $sectionId;
        $entry->typeId = $typeId;
        $entry->authorId = $userId;
        $entry->setFieldValues($fields);
        return $entry;
    }

    /*
     * Generate pdf report
     * @param [Array] $data data array for pdf content
     * @return string pdf file temp path (Error return false)
     * */
    public function getPdfReport($data, $saveFile = false)
    {
        $pdfPath = "";
        if (empty($data)) {
            return false;
        }

        $pdfHtml = Craft::$app->getView()->renderTemplate('planning/downloadable-pdf', $data);

        $defaultConfig = (new ConfigVariables)->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables)->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new mPDF([
            'fontDir' => array_merge($fontDirs, [
                CRAFT_BASE_PATH . '/assets/font',
            ]),
            'fontdata' => $fontData + [
                    'filsonpro' => [
                        'R' => 'Filson-Pro-Regular.ttf',
                        'I' => 'Filson-Pro-Regular-Italic.ttf',
                    ]
                ],
            'default_font' => 'filsonpro',
        ]);

        $mpdf->WriteHTML($pdfHtml);

        if ($saveFile) {
            $pdffileName = 'NDIS Planning' . time() . '.pdf';
            $mpdf->Output($pdffileName, 'F');
            return $pdffileName;
        } else {
            $mpdf->Output('NDIS Planning.pdf', 'D');
        }
        return $pdfPath;
    }

    public function preparePdfStep1($step1)
    {
        $data = [];
        $step1Options = $this->getStep1FieldSettings();
        foreach ($step1Options as $key => $each) {
            if (!empty($step1[$each['handle']])) {
                $data[] = array_merge($each, array('value' => $step1[$each['handle']]));
            }
        }
        return $data;
    }

    public function preparePdfStep2($step2)
    {
        $data = [];
        $step2Options = YodhubModule::$instance->planning->getStep2EverydayFieldSettings();
        foreach ($step2Options as $key => $each) {
            $step2fields = [];
            if (!empty($each['fields'])) {
                foreach ($each['fields'] as $field) {

                    if (!empty($step2[$field['handle']])) {
                        $step2fields[] = array_merge(
                            $field,
                            array('value' => $step2[$field['handle']])
                        );
                    }
                }
                $data[] = array_merge(
                    array(
                        'heading' => $each['heading'],
                        'group' => @$each['group'],
                    ),
                    $step2fields
                );
            } else {
                if (!empty($step2[$each['handle']])) {
                    $data[] = array_merge(
                        $each,
                        array('value' => $step2[$each['handle']])
                    );
                }
            }
        }
        return $data;
    }

    public function preparePdfSteps()
    {
        $data = [];
        $steps = $this->getStepBasicDetails();
        foreach ($steps as $step) {
            $data[$step['stepId']] = [
                'title' => $step['title'],
                'resultSummary' => $step['resultSummary']
            ];
        }
        return $data;
    }

    public function preparePdfAnalysisAnswers($goalsAnalysisAnswers, $goalsRanking)
    {
        $data = [];

        foreach ($goalsAnalysisAnswers as $stepId => $step) {
            foreach ($step as $key => $each) {
                $entry = \craft\elements\Category::find()
                    ->id($key)
                    ->one();
                if ($entry) {
                    $data[$stepId]['answers'][$entry->goalAnalysisType->label][] = [
                        "title" => $entry->title,
                        "value" => $each
                    ];
                }
            }
        }

        foreach ($goalsRanking as $stepId => $ranking) {
            foreach ($ranking as $key => $each) {
                $entry = \craft\elements\Category::find()
                    ->id($key)
                    ->one();
                if ($entry) {
                    $data[$stepId]['ranking'][] = [
                        "title" => $entry->title,
                        "note" => $each['notes']
                    ];
                }
            }
        }


        return $data;
    }

    public function preparePdfDefinition()
    {
        $data = [];
        $entries = \craft\elements\Category::find()
            ->group("scoreDefinitions")
            ->all();
        foreach ($entries as $entry) {
            $data[$entry->goalAnalysisType->label][] = $entry->title;
        }

        return $data;
    }

    public function checkreCaptcha($response)
    {
        $curlx = curl_init();

        curl_setopt($curlx, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($curlx, CURLOPT_HEADER, 0);
        curl_setopt($curlx, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlx, CURLOPT_POST, 1);
        $secret = Craft::$app->config->general->reCaptchaSecretKey;
        $post_data =
            [
                'secret' => $secret, //<--- your reCaptcha secret key
                'response' => $response
            ];

        curl_setopt($curlx, CURLOPT_POSTFIELDS, $post_data);

        $resp = json_decode(curl_exec($curlx));

        curl_close($curlx);
        
        if ($resp->success) {
            return true;
            //success!
        } else {
            return false;
        }
    }

    public function getYourPlanPageUrl() {
        $yourPlanPageEntry = craft\elements\Entry::find()
            ->section('pages')
            ->customTemplate('your-plan')
            ->one();
        if($yourPlanPageEntry) {
            return $yourPlanPageEntry->url;
        }
    }

    public function getPlanningLandingPageUrl() {
        $planningLandingEntry = craft\elements\Entry::find()
            ->section('pages')
            ->type('ndisPlanningLandingPage')
            ->one();
        if($planningLandingEntry) {
            return $planningLandingEntry->url;
        }
    }
}
