<?php
/**
 * yodhub module for Craft CMS 3.x
 *
 * Custom module for Dementia Australia - YOD Hub
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2020 Yump
 */

namespace modules\yodhubmodule\services;

use modules\yodhubmodule\YodhubModule;
use craft\helpers\ElementHelper;
use Craft;
use craft\base\Component;

/**
 * YodhubModuleService Service
 *
 * All of your module’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other modules can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Yump
 * @package   YodhubModule
 * @since     1.0.0
 */
class YodhubModuleService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin/module file, call it like this:
     *
     *     YodhubModule::$instance->yodhub->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }

    /** the middle banner service to get data */
    public function getMiddleBanner($entry)
    {
        if (!empty($entry['middleBanner'])) {
            $banner = $entry->middleBanner;
            // $this->dump($banner);

            // get textContent block and media block
            $textContentBlock = null;
            $mediaBlock = null;
            // dump($banner->level(1)->all()->id );
            foreach ($banner->level(1)->all() as $block) {

                $handle = $block->getType()->handle;
                if ($handle == 'textContent') {
                    $textContentBlock = $block;
                    // if($block->_nextElement == false){
                    //     dump($block);
                    // }
                    // dump($block->id);
                    // dump($block->nextSibling);
                } else if ($handle == 'media') {
                    $mediaBlock = $block;
                    // if($block->_nextElement == false){
                    //     dump($block);
                    // }
                    // dump($block->id);
                }
            }

            // if no even textContent, the banner will be considered empty, even if media block is present.
            // In this banner we only have one option for the image and the featured image is not an option
            if ($textContentBlock) {
                $heading = null;
                $summary = null;
                $buttons = null;
                $textContent = null;
                // getting data of textContent including heading and summary and buttons
                $textContent = $textContentBlock->bannerTextContent;
                foreach ($textContentBlock->getChildren()->all() as $block) {
                    $handle = $block->getType()->handle;
                    if ($handle == 'buttons') {
                        foreach ($block->getChildren()->all() as $ctaBlock) {
                            if ($ctaBlock->getType()->handle == 'callToAction') {
                                if ($buttons === null) {
                                    $buttons = [];
                                }
                                $buttons[] = $ctaBlock->cta;
                            }
                        }
                    }
                }
                // getting data of media / this is similar to top banner
                $media = null;
                if ($mediaBlock) {
                    foreach ($mediaBlock->getChildren()->all() as $block) {
                        $handle = $block->getType()->handle;
                        if ($handle == 'image') {
                            $imageSource = null;
                            if (!empty($block['image'])) {
                                $imageSource = $block->image;
                                $image = $imageSource->one();
                                if ($image) {
                                    $media = [
                                        'type' => 'image',
                                        'content' => $image,
                                    ];
                                }
                            }

                        } else if ($handle == 'video') {
                            if (!empty($block['video'])) {
                                $media = [
                                    'type' => 'video',
                                    'content' => $block->video,
                                    'viideoBackgroundImage' => $block->image->one(),
                                    'settings' => [
                                        'transcriptEntry' => $block->videoTranscript ? $block->videoTranscript->one() : null,
                                        'videoHtmlTitle' => $block->videoHtmlTitle,
                                    ],
                                ];
                            }
                        }
                    }
                }

                return [
                    'textContent' => $textContent,
                    'buttons' => $buttons,
                    'media' => $media,
                ];
            }
        }
    }

// plan landing banner service

    public function getplanLandingBanner($entry)
    {
        if (!empty($entry['planLandingBanner'])) {
            $banner = $entry->planLandingBanner;

            // get textContent block and media block
            $textContentBlock = null;
            $mediaBlock = null;
            // dump($banner->level(1)->all()->id );
            foreach ($banner->level(1)->all() as $block) {

                $handle = $block->getType()->handle;
                if ($handle == 'textContent') {
                    $textContentBlock = $block;
                    // if($block->_nextElement == false){
                    //     dump($block);
                    // }
                    // dump($block->id);
                    // dump($block->nextSibling);
                } else if ($handle == 'media') {
                    $mediaBlock = $block;
                    // if($block->_nextElement == false){
                    //     dump($block);
                    // }
                    // dump($block->id);
                }
            }

            // if no even textContent, the banner will be considered empty, even if media block is present.
            // In this banner we only have one option for the image and the featured image is not an option
            if ($textContentBlock) {
                $createPlanTitle = null;
                $savedPlanTitle = null;
                $createPlanSummary = null;
                $savedPlanSummary = null;
                foreach ($textContentBlock->getChildren()->all() as $block) {
                    $handle = $block->getType()->handle;
                    if ($handle == 'createPlan') {
                        $createPlanTitle = $block->heading;
                        $createPlanSummary = $block->summary;
                    }
                    if ($handle == 'savedPlan') {
                        $savedPlanTitle = $block->heading;
                        $savedPlanSummary = $block->summary;
                    }
                }
                // getting data of media / this is similar to top banner
                $media = null;
                if ($mediaBlock) {
                    foreach ($mediaBlock->getChildren()->all() as $block) {
                        $handle = $block->getType()->handle;
                        if ($handle == 'image') {
                            $imageSource = null;
                            if (!empty($block['image'])) {
                                $imageSource = $block->image;
                                $image = $imageSource->one();
                                if ($image) {
                                    $media = [
                                        'type' => 'image',
                                        'content' => $image,
                                    ];
                                }
                            }

                        } else if ($handle == 'video') {
                            if (!empty($block['video'])) {
                                $media = [
                                    'type' => 'video',
                                    'content' => $block->video,
                                    'viideoBackgroundImage' => $block->image->one(),
                                    'settings' => [
                                        'transcriptEntry' => $block->videoTranscript ? $block->videoTranscript->one() : null,
                                        'videoHtmlTitle' => $block->videoHtmlTitle,
                                    ],
                                ];
                            }
                        }
                    }
                }

                return [
                    'createPlanTitle' => $createPlanTitle,
                    'savedPlanTitle' => $savedPlanTitle,
                    'createPlanSummary' => $createPlanSummary,
                    'savedPlanSummary' => $savedPlanSummary,
                    'media' => $media,
                ];
            }
        }
    }
    // create slug for anchor points and links
    public function createSlug($text) {
        return ElementHelper::createSlug($text);
    }

    public function getAnchorLinks($entry) {

        $anchor_links = [];
        if($entry instanceof \craft\elements\Entry) {
            $blocks = $entry['body'];
        } else {
            $blocks = null;
        }

        if($blocks) {
            foreach($blocks->all() as $block) {
                foreach($block->children->all() as $childBlock ) {
                    if ($childBlock->type == 'anchorPoint') {
                        $anchorLink = $this->createSlug($childBlock->label);
                        // echo $anchorLink;
                        $anchor_links[] = ['link'=>$anchorLink, 'label'=>$childBlock->label];
                    };
                }
            }
        }

        if(count($anchor_links) == 0) {
            return false;
        }

        return $anchor_links;
    }
}
