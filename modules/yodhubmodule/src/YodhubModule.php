<?php
/**
 * yodhub module for Craft CMS 3.x
 *
 * Custom module for Dementia Australia - YOD Hub
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2020 Yump
 */

namespace modules\yodhubmodule;

use modules\yodhubmodule\assetbundles\yodhubmodule\YodhubModuleAsset;
use modules\yodhubmodule\services\YodhubModuleService as YodhubModuleServiceService;
use modules\yodhubmodule\variables\YodhubModuleVariable;
use modules\yodhubmodule\variables\YodhubModulePlanningVariable;
use modules\yodhubmodule\twigextensions\YodhubModuleTwigExtension;
use modules\yodhubmodule\fields\YodhubModuleField as YodhubModuleFieldField;
use modules\yodhubmodule\utilities\YodhubModuleUtility as YodhubModuleUtilityUtility;
use modules\yodhubmodule\widgets\YodhubModuleWidget as YodhubModuleWidgetWidget;

use Craft;
use craft\events\RegisterTemplateRootsEvent;
use craft\events\TemplateEvent;
use craft\i18n\PhpMessageSource;
use craft\web\View;
use craft\console\Application as ConsoleApplication;
use craft\web\UrlManager;
use craft\services\Elements;
use craft\services\Fields;
use craft\services\Utilities;
use craft\web\twig\variables\CraftVariable;
use craft\services\Dashboard;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\base\Module;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Yump
 * @package   YodhubModule
 * @since     1.0.0
 *
 * @property  YodhubModuleServiceService $yodhubModuleService
 */
class YodhubModule extends Module
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this module class so that it can be accessed via
     * YodhubModule::$instance
     *
     * @var YodhubModule
     */
    public static $instance;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function __construct($id, $parent = null, array $config = [])
    {
        Craft::setAlias('@modules/yodhubmodule', $this->getBasePath());
        $this->controllerNamespace = 'modules\yodhubmodule\controllers';

        // Translation category
        $i18n = Craft::$app->getI18n();
        /** @noinspection UnSafeIsSetOverArrayInspection */
        if (!isset($i18n->translations[$id]) && !isset($i18n->translations[$id.'*'])) {
            $i18n->translations[$id] = [
                'class' => PhpMessageSource::class,
                'sourceLanguage' => 'en-US',
                'basePath' => '@modules/yodhubmodule/translations',
                'forceTranslation' => true,
                'allowOverrides' => true,
            ];
        }

        // Base template directory
        Event::on(View::class, View::EVENT_REGISTER_CP_TEMPLATE_ROOTS, function (RegisterTemplateRootsEvent $e) {
            if (is_dir($baseDir = $this->getBasePath().DIRECTORY_SEPARATOR.'templates')) {
                $e->roots[$this->id] = $baseDir;
            }
        });

        // Set this as the global instance of this module class
        static::setInstance($this);

        parent::__construct($id, $parent, $config);
    }

    /**
     * Set our $instance static property to this class so that it can be accessed via
     * YodhubModule::$instance
     *
     * Called after the module class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$instance = $this;

        // Load our AssetBundle
        if (Craft::$app->getRequest()->getIsCpRequest()) {
            Event::on(
                View::class,
                View::EVENT_BEFORE_RENDER_TEMPLATE,
                function (TemplateEvent $event) {
                    try {
                        Craft::$app->getView()->registerAssetBundle(YodhubModuleAsset::class);
                    } catch (InvalidConfigException $e) {
                        Craft::error(
                            'Error registering AssetBundle - '.$e->getMessage(),
                            __METHOD__
                        );
                    }
                }
            );
        }

        // Add in our Twig extensions
        Craft::$app->view->registerTwigExtension(new YodhubModuleTwigExtension());

        // Add in our console commands
        if (Craft::$app instanceof ConsoleApplication) {
            $this->controllerNamespace = 'modules\yodhubmodule\console\controllers';
        }

        // Register our site routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['siteActionTrigger1'] = 'yodhub-module/default';
            }
        );

        // Register our CP routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['cpActionTrigger1'] = 'yodhub-module/default/do-something';
            }
        );

        // Register our elements
        Event::on(
            Elements::class,
            Elements::EVENT_REGISTER_ELEMENT_TYPES,
            function (RegisterComponentTypesEvent $event) {
            }
        );

        // Register our fields
        // Event::on(
        //     Fields::class,
        //     Fields::EVENT_REGISTER_FIELD_TYPES,
        //     function (RegisterComponentTypesEvent $event) {
        //         $event->types[] = YodhubModuleFieldField::class;
        //     }
        // );

        // Register our utilities
        Event::on(
            Utilities::class,
            Utilities::EVENT_REGISTER_UTILITY_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = YodhubModuleUtilityUtility::class;
            }
        );

        // Register our widgets
        Event::on(
            Dashboard::class,
            Dashboard::EVENT_REGISTER_WIDGET_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = YodhubModuleWidgetWidget::class;
            }
        );

        // Register our variables
        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('yodhub', YodhubModuleVariable::class);
                $variable->set('yodhubPlanning', YodhubModulePlanningVariable::class);
            }
        );

/**
 * Logging in Craft involves using one of the following methods:
 *
 * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
 * Craft::info(): record a message that conveys some useful information.
 * Craft::warning(): record a warning message that indicates something unexpected has happened.
 * Craft::error(): record a fatal error that should be investigated as soon as possible.
 *
 * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
 *
 * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
 * the category to the method (prefixed with the fully qualified class name) where the constant appears.
 *
 * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
 * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
 *
 * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
 */
        // Craft::info(
        //     Craft::t(
        //         'yodhub-module',
        //         '{name} module loaded',
        //         ['name' => 'yodhub']
        //     ),
        //     __METHOD__
        // );

        /**
         * Custom logging config
         *
         * Usage: same as Craft logging usage:
         * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
         * Craft::info(): record a message that conveys some useful information.
         * Craft::warning(): record a warning message that indicates something unexpected has happened.
         * Craft::error(): record a fatal error that should be investigated as soon as possible.
         *
         * E.g. Craft::error('my message', __METHOD__);
         * 
         */
        // Create a new file target
        $fileTarget = new \craft\log\FileTarget([
            'logFile' => '@storage/logs/yodhub.log',
            'categories' => ['modules\yodhubmodule\*']
        ]);
        // Add the new target file target to the dispatcher
        Craft::getLogger()->dispatcher->targets[] = $fileTarget;


        /**
         * Before element saved
         */
        Event::on(
            \craft\services\Elements::class,
            \craft\services\Elements::EVENT_BEFORE_SAVE_ELEMENT,
            function (\craft\events\ElementEvent $event) {
                $element = $event->element;
                $isNew = $event->isNew;
                $request = Craft::$app->getRequest();
                $isSiteRequest = Craft::$app->getRequest()->getIsSiteRequest();

                /**
                 * reCaptcha check for front end user sign up requests
                 */
                if(
                    $element instanceof \craft\elements\User // is entry
                    and $isNew
                    and $isSiteRequest
                    and !$element->getIsRevision() // is not revision
                    and !$element->getIsDraft() // is not draft
                ) {
                    $captcha = $request->getBodyParam('g-recaptcha-response');
                    if(empty($captcha) or !YodhubModule::$instance->planning->checkreCaptcha($captcha)) {
                        throw new \yii\web\HttpException(500, "Invalid reCaptcha request. If you are a human, please make sure you have ticked the - I'm not a robot - box.");
                    }
                }
            }
        );

        /**
         * Listen to element saved event
         */
        /**
         * After entry save event
         */
        Event::on(
            \craft\services\Elements::class,
            \craft\services\Elements::EVENT_AFTER_SAVE_ELEMENT,
            function (\craft\events\ElementEvent $event) {
                $element = $event->element;
                $isNew = $event->isNew;
                $request = Craft::$app->getRequest();
                $isPlanningToolUser = $request->getBodyParam('planningToolUser');

                // try {
                //     throw new \Exception();
                // } catch (\Exception $e) {
                //     Craft::debug(
                //         'New element saved - ID: ' . $element->id 
                //         . "\n isNew: " . ($isNew ? 'true' : 'false') 
                //         . "\nis User element: " . ($element instanceof \craft\elements\User ? 'true' : 'false')
                //         . "\n isInGroup('planningToolUsers'): " . ($element->isInGroup('planningToolUsers') ? 'true' : 'false') 
                //         . "\n getIsRevision: " . ($element->getIsRevision() ? 'true' : 'false') 
                //         . "\n getIsDraft: " . ($element->getIsDraft() ? 'true' : 'false') 
                //         . "\n propagating: " . ($element->propagating ? 'true' : 'false') 
                //         . "\n resaving: " . ($element->resaving ? 'true' : 'false')
                //         . "\nisPlanningToolUser". ($isPlanningToolUser ?? 'false')
                //         . "\nPost Params: \n" . print_r($_POST, true)
                //         // . "\n\nEvent: " . print_r($event, true) 
                //         . "\nTrace:\n\n" .$e->getTraceAsString(),
                //         __METHOD__
                //     );
                // }

                // listen for new user signup event and create a new plan as well as link the user to it
                if(
                    $element instanceof \craft\elements\User // is entry
                    and $isNew
                    and $isPlanningToolUser // is in 'pages' section
                    and !$element->getIsRevision() // is not revision
                    and !$element->getIsDraft() // is not draft
                ) {
                    Craft::info('New planning tool user created: ' . $element->id, __METHOD__); 
                    
                    // try {
                    //     throw new \Exception();
                    // } catch (\Exception $e) {
                    //     Craft::debug(
                    //         'New planning tool user created - ID: ' . $element->id . "Trace:\n\n" .$e->getTraceAsString(),
                    //         __METHOD__
                    //     );
                    // }
                    
                    // create a new plan entry and assign it to the current user
                    $currentUserId = $element->id;
                    $section = Craft::$app->sections->getSectionByHandle('plans');
                    $sectionId = $section->id;
                    $typeId = $section->getEntryTypes()[0]->id;
                    $fields = array(
                        'currentStep' => 1,
                        'planningToolUser' => array(
                            $currentUserId
                        ),
                    );
                    $plan = new \craft\elements\Entry();
                    $plan->sectionId = $sectionId;
                    $plan->typeId = $typeId;
                    $plan->authorId = $currentUserId;
                    $plan->setFieldValues($fields);
                    $success = Craft::$app->elements->saveElement($plan);

                    if(!$success) {
                        Craft::error("Cannot create new plan entry for new user [ID: " . $currentUserId . "]. Plan ID: [" . $plan->id . "]! Errors:\n" . print_r($plan->getErrors(), true), __METHOD__);
                        throw new \yii\web\HttpException(500, "There was an error when creating your profile.");
                    } else {
                        Craft::info("Successfully created a new plan entry and assigned it to the user!"
                            . "\nPlan Entry ID: " . $plan->id
                            . "\nUser ID: " . $currentUserId
                            , __METHOD__);
                    }
                }
            }
        );

    }

    // Protected Methods
    // =========================================================================
}
