<?php

namespace modules\yodhubmodule\helpers\planning;

class Constants
{
	/**
	 * Please make this list up to date with all the general question fields created via the CMS. The fields are for the general questions of the planning tool questionnaire. Usually we put them under the field group "NDIS Planning Tool General Questions"
	 */
	const GENERAL_QUESTIONS_FIELD_HANDLES = array(
        'informalSupports',
        'formalSupports',
        'healthRelatedSupports',
        'otherImportantConnections',
        'whatIDoMonday',
        'supportFromMonday',
        'whatIDoTuesday',
        'supportFromTuesday',
        'whatIDoWednesday',
        'supportFromWednesday',
        'whatIDoThursday',
        'supportFromThursday',
        'whatIDoFriday',
        'supportFromFriday',
        'whatIDoSaturday',
        'supportFromSaturday',
        'whatIDoSunday',
        'supportFromSunday',
        'occasionalActivities',
        'occasionalActivitiesSupportFrom',
    );

    const STEP1_FIELD_SETTINGS = array(
        array(
            'label' => 'Informal supports',
            'handle' => 'informalSupports',
            'required' => true,
            'instructions' => 'For example:<br><br>“Unpaid support people such as your Wife, Husband, Sister, Friend, Family, friends, neighbours, social groups”',
        ),
        array(
            'label' => 'Formal supports',
            'handle' => 'formalSupports',
            'required' => true,
            'instructions' => 'For example:<br><br>“Paid service providers such Cleaner, Gardener, Meal preparation”',
        ),
        array(
            'label' => 'Health-related supports',
            'handle' => 'healthRelatedSupports',
            'required' => true,
            'instructions' => 'For example:<br><br>“Professional service providers who support you with your health such as a Doctor, specialist, Occupational therapist, Physiotherapist”',
        ),
        array(
            'label' => 'Other important connections',
            'handle' => 'otherImportantConnections',
            'required' => true,
            'instructions' => 'For example:<br><br>“Organisations or community groups that you are connected with such as Dementia Australia, Sporting clubs, Church”',
        ),
    );

    const STEP2_EVERYDAY_FIELD_SETTINGS = array(
        array(
            'heading' => 'Monday',
            'group' => 'Monday',
            'fields' => array(
                array(
                    'label' => 'What I do',
                    'handle' => 'whatIDoMonday',
                    'required' => true,
                    'instructions' => 'For example:<br><br>“Volunteer at animal shelter (9am-3pm)”<br>“Take medication”',
                ),
                array(
                    'label' => 'To do these things I get support from',
                    'handle' => 'supportFromMonday',
                    'required' => true,
                    'instructions' => 'For example:<br><br>“My partner drives me to and from the animal shelter.”<br>“I have a reminder set on my phone to take my medication.”',
                ),
            ),
        ),
        array(
            'heading' => 'Tuesday',
            'group' => 'Tuesday',
            'fields' => array(
                array(
                    'label' => 'What I do',
                    'handle' => 'whatIDoTuesday',
                    'required' => true,
                ),
                array(
                    'label' => 'To do these things I get support from',
                    'handle' => 'supportFromTuesday',
                    'required' => true,
                ),
            ),
        ),
        array(
            'heading' => 'Wednesday',
            'group' => 'Wednesday',
            'fields' => array(
                array(
                    'label' => 'What I do',
                    'handle' => 'whatIDoWednesday',
                    'required' => true,
                ),
                array(
                    'label' => 'To do these things I get support from',
                    'handle' => 'supportFromWednesday',
                    'required' => true,
                ),
            ),
        ),
        array(
            'heading' => 'Thursday',
            'group' => 'Thursday',
            'fields' => array(
                array(
                    'label' => 'What I do',
                    'handle' => 'whatIDoThursday',
                    'required' => true,
                ),
                array(
                    'label' => 'To do these things I get support from',
                    'handle' => 'supportFromThursday',
                    'required' => true,
                ),
            ),
        ),
        array(
            'heading' => 'Friday',
            'group' => 'Friday',
            'fields' => array(
                array(
                    'label' => 'What I do',
                    'handle' => 'whatIDoFriday',
                    'required' => true,
                ),
                array(
                    'label' => 'To do these things I get support from',
                    'handle' => 'supportFromFriday',
                    'required' => true,
                ),
            ),
        ),
        array(
            'heading' => 'Saturday',
            'group' => 'Saturday',
            'fields' => array(
                array(
                    'label' => 'What I do',
                    'handle' => 'whatIDoSaturday',
                    'required' => true,
                ),
                array(
                    'label' => 'To do these things I get support from',
                    'handle' => 'supportFromSaturday',
                    'required' => true,
                ),
            ),
        ),
        array(
            'heading' => 'Sunday',
            'group' => 'Sunday',
            'fields' => array(
                array(
                    'label' => 'What I do',
                    'handle' => 'whatIDoSunday',
                    'required' => true,
                ),
                array(
                    'label' => 'To do these things I get support from',
                    'handle' => 'supportFromSunday',
                    'required' => true,
                ),
            ),
        ),
        array(
            'heading' => 'Are there any activities that you participate in occasionally?',
            // 'group' => 'occasionalActivities',
            'fields' => array(
                array(
                    'label' => 'What I do',
                    'handle' => 'occasionalActivities',
                    'required' => true,
                    'instructions' => 'For example:<br><br>“Every second Saturday I go golfing with friends.”<br>“I like to go to the movies once a month.”',
                ),
                array(
                    'label' => 'To do these things I get support from',
                    'handle' => 'occasionalActivitiesSupportFrom',
                    'required' => true,
                    'instructions' => 'For example:<br><br>“My husband”<br>“My daughter”<br>“My counsellor”',
                ),
            ),
        ),
    );
}