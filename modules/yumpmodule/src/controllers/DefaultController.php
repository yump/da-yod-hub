<?php
/**
 * yump module for Craft CMS 3.x
 *
 * Yump module for Craft 3
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\yumpmodule\controllers;

use modules\yumpmodule\YumpModule;

use modules\yumpmodule\gears\cache\adapters\Nav as YumpNavAdapter;

use Craft;
use craft\web\Controller;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your module’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Yump
 * @package   YumpModule
 * @since     1.0.0
 */
class DefaultController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something', 'db-backup', ];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our module's index action URL,
     * e.g.: actions/yump-module/default
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'Welcome to the DefaultController actionIndex() method';

        return $result;
    }

    /**
     * Handle a request going to our module's actionDoSomething URL,
     * e.g.: actions/yump-module/default/do-something
     *
     * @return mixed
     */
    public function actionDoSomething()
    {
        $result = 'Welcome to the DefaultController actionDoSomething() method';

        return $result;
    }

    /**
     * Allow cron jobs to run this script to auto-backup database. 
     * - Backup file will be automatically created under /craft/storage/backup (by Craft). 
     * - A download will also be automatically triggered. 
     * - Using Yump's DB backup script for Cron job, it will be automatically saved to a Google Doc folder devs having access to.
     *
     * URL to run: [host]/actions/yump-module/default/db-backup?a=[$PASSWORD]
     * @return null
     */
    public function actionDbBackup() {
        if (YumpModule::$instance->yump->checkScriptPermission(true)) {
            try {
                $backupPath = Craft::$app->getDb()->backup(); // by running this line, Craft will automatically create a backup under /craft/storage/backup
            } catch (\Throwable $e) {
                throw new \Exception('Could not create backup: ' . $e->getMessage());
            }

            if(is_file($backupPath)) {
                return Craft::$app->getResponse()->sendFile($backupPath);
            } else {
                die('Invalid backup file: ' . $backupPath);
            }
        } else {
            die('You have no right to run this script, or the security settings on the server are incorrectly configured.');
        }
    }

    /**
     * This action can used to force update the nav cache. It can be used by admin in CP (need to build a separate Yump plugin page in CP for them to do so, or can simply give them an URL); or, it can be used by Cron job to automatically update it frequently.
     *
     * By default, this action does not allow anonymous. If you want it to be accessible by cron jobs, you will need to allow anonymous (at the beginning of this file), and also wrap it with a password check (refer to the actionDbBackup function above)
     * @return [type] [description]
     */
    public function actionUpdateNavCache() {
        $result = YumpModule::$instance->yump->forceUpdateNav();
        if(!empty($result['error'])) {
            print_r($result['error']);
        }
        exit();
    }

    /**
     * /actions/yump-module/default/test
     * @return [type] [description]
     */
    public function actionTest() {
        $key = 'site-navigation';
        // $contentArr = [
        //     'arr' => [
        //         'sdf',
        //         'ioie'
        //     ],
        //     'option' => true,
        //     'text' => 'hello',
        // ];
        // $content = json_encode($contentArr, JSON_PRETTY_PRINT);

        // YumpModule::$instance->yump->dump(YumpModule::$instance->cache->getCraftCachedContent($key));

        // YumpModule::$instance->cache->setCraftCachedContent($key, $content);

        // YumpModule::$instance->cache->setYumpCachedContent($key, $content, array(
        //     'usePublicFolder' => true,
        //     // 'customFolder' => CRAFT_BASE_PATH . "/web/yump/cache/test"
        // ));

        // YumpModule::$instance->yump->dump(YumpModule::$instance->cache->getYumpCachedContent($key, false, array(
        //     'usePublicFolder' => true,
        //     // 'customFolder' => CRAFT_BASE_PATH . "/web/yump/cache/test"
        // )));

        // $nav = YumpModule::$instance->yump->getNav();

        // $nav = YumpModule::$instance->cache->getCraftCachedContent($key);

        // YumpModule::$instance->yump->dump($nav);
        die();
    }
}
