/**
 * yump module for Craft CMS
 *
 * YumpModuleWidget Widget JS
 *
 * @author    Yump
 * @copyright Copyright (c) 2019 Yump
 * @link      https://yump.com.au
 * @package   YumpModule
 * @since     1.0.0
 */
