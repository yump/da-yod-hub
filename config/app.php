<?php
/**
 * Yii Application Config
 *
 * Edit this file at your own risk!
 *
 * The array returned by this file will get merged with
 * vendor/craftcms/cms/src/config/app.php and app.[web|console].php, when
 * Craft's bootstrap script is defining the configuration for the entire
 * application.
 *
 * You can define custom modules and system components, and even override the
 * built-in system components.
 *
 * If you want to modify the application config for *only* web requests or
 * *only* console requests, create an app.web.php or app.console.php file in
 * your config/ folder, alongside this one.
 */

return [
    'modules' => [
        'my-module' => \modules\Module::class,
        'yump-module' => [
            'class' => \modules\yumpmodule\YumpModule::class,
            'components' => [
                'yump' => [
                    'class' => 'modules\yumpmodule\services\YumpModuleService',
                ],
                'blackbox' => [
                    'class' => 'modules\yumpmodule\services\YumpModuleBlackboxService',
                ],
                'cache' => [
                    'class' => 'modules\yumpmodule\services\YumpModuleCacheService',
                ],
            ],
        ],
        'example-module' => [
            'class' => \modules\examplemodule\ExampleModule::class,
            'components' => [
                'example' => [
                    'class' => 'modules\examplemodule\services\ExampleModuleService',
                ],
            ],
        ],
         'yodhub-module' => [
            'class' => \modules\yodhubmodule\YodhubModule::class,
            'components' => [
                'yodhub' => [
                    'class' => 'modules\yodhubmodule\services\YodhubModuleService',
                ],
                'planning' => [
                    'class' => 'modules\yodhubmodule\services\YodhubModulePlanningService',
                ],
            ],
        ],
    ],
    'bootstrap' => ['yump-module', 'example-module', 'yodhub-module'],
];
