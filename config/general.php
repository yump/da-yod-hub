<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see \craft\config\GeneralConfig
 */

return [
    // Global settings
    '*' => [
        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 1,

        // Whether generated URLs should omit "index.php"
        'omitScriptNameInUrls' => true,

        // error page templates will be located under template root with '_' as prefix
        'errorTemplatePrefix' => "_errors/",

        // Control Panel trigger word
        'cpTrigger' => 'admin',

        // Whether Craft should allow system and plugin updates in the Control Panel, and plugin installation from the Plugin Store.
        'allowUpdates' => false,

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('SECURITY_KEY'),

        // Whether to save the project config out to config/project.yaml
        // (see https://docs.craftcms.com/v3/project-config.html)
        'useProjectConfigFile' => false,

        // if not provided here, Craft will use the one set in CMS (deprecated, use aliases instead)
        // 'siteUrl' => sprintf(
        //     "%s://%s",
        //     isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        //     $_SERVER['SERVER_NAME']
        // ), 

        'aliases' => [
            '@web' => craft\helpers\App::env('DEFAULT_SITE_URL'),
            '@webroot' => CRAFT_BASE_PATH,
        ],

        // Enable CSRF Protection (recommended)
        'enableCsrfProtection' => true,

        // login path
        'loginPath' => 'admin/login',

        // 'errorTemplatePrefix' => "_errors/",

        // 'defaultImageQuality' => 100,
        
        'overridePhpSessionLocation' => true,
        'userSessionDuration' => 10800, // 3 hours

        'yumpCurlCacheFolder' => CRAFT_BASE_PATH . '/yump/private/cache/curl',

        // custom script permission control
        'controllerActionsPassword' => 'QUpX67EkiETU7Rg',
        'controllerActionsAllowedIps' => array(
            '127.0.0.1', 
            '203.191.201.182',        // Epic office
            '202.91.44.62',           // depo8
            '43.241.54.230',          // staging.yump.com.au (new server)
        ),

        // Card default settings
        'imageCardImagerDefaultSettings' => array(
            'width' => 792,
            'ratio' => 16/9,
        ),
        'defaultCardCtaText' => 'Learn more',
        'defaultTestimonialButtonText' => 'Read full story',
        'imageCardPlaceholderUrl' => false, // if path provided, we will use the placeholder image when card image is missing. The placeholder image's dimension should follow the 'imageCardImagerDefaultSettings'

        // cache
        'siteNavigationCacheKey' => 'site-navigation',
        'navShouldIncludeHomepage' => true, // if set to true, then the cached navigation content will include homepage at the front of the navItems list, when running the getNav() function under YumpModuleService. Please note, if we don't have homepage in the nav list, then the breadcrumb should include Home crumb; otherwise it shouldn't.

        // social media
        // define which social media links are shown. These URLs must be defined in SEOMatic under Social Media -> 'Same as Links' in order to display.
        'activeSocialMediaIcons' => array(
            array(
                'handle' => 'facebook',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'facebook-square',
            ),
            array(
                'handle' => 'twitter',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'twitter-square',
            ),
            array(
                'handle' => 'youtube',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'youtube-square',
            ),
            array(
                'handle' => 'instagram',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'instagram',
            ),
            array(
                'handle' => 'linkedin',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'linkedin',
            ),
        ),

        // Page IDs
        'newsLandingPageId' => 491,

        'planningStepImagerSettings' => array(
            'width' => 1292,
        ),

        'autoLoginAfterAccountActivation' => true,
        'postLogoutRedirect' => '/planning/landing',

        // Google reCaptcha
        'reCaptchaSiteKey' => '6LdjngAVAAAAANzHzGhy99JXJTtF8QaSHR9JU31E',
        'reCaptchaSecretKey' => '6LdjngAVAAAAAAB6PQSKAkdehYJgxdMjJKBzt7aD',
    ],

    // Dev environment settings
    'dev' => [
        // Dev Mode (see https://craftcms.com/guides/what-dev-mode-doesp)
        'devMode' => true,
        'allowUpdates' => true,
		'verify' => false, 
        'testToEmailAddress' => array(
        'devs@yump.com.au' => 'Yump Devs'
        ),
    ],

    // Staging environment settings
    'staging' => [
        // Set this to `false` to prevent administrative changes from being made on staging
        'allowAdminChanges' => true,

        // Dev Mode (enabled when ?devMode=1 querystring is present)
        'devMode' => !empty($_GET['devMode']),
        'enableLoginWallOnDocs' => true,
    ],

    // Production environment settings
    'production' => [
        // Set this to `false` to prevent administrative changes from being made on production
        'allowAdminChanges' => true,

        'enableLoginWallOnDocs' => true,
    ],
];
