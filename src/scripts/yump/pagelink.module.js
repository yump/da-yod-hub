$(document).ready(function () {
    $(".page-links__item").click(function () {
        var urlLink = $(this).data("link");
        var pos = $(urlLink).offset().top - 10;
        $('html, body').animate({
            scrollTop: pos
        }, 800);
    });
});