// const $form = $('#questionnaire');
// const pageTitle = document.title;
const urlPath = window.location.pathname;

var planningVue = new Vue({
    el: '#planning-vue',
    delimiters: ['{*', '*}'],
    data() {
        return {
            info: null,
            initialising: true,
            initErrored: false,
            saving: false,
            showSavedScreen: false,
            loggingOut: false,
            updating: false,

            csrfTokenName: window.csrfTokenName,
            csrfTokenValue: window.csrfTokenValue,

            formInputs: {
                general: window.generalQuestionFields,
                goalsAnalysis: window.goalAnalysisQuestionFields,
                optionalGoals: window.optionalGoalsFields,
            },

            viewingStep: null,
            furthestViewableStep: null,

            daysSelector: [],
            sourceDay: null,

            goalsAnalysisAnswersHaveChanged: {},
            stepsThatNeedsSave: {}, // indicates whether a step has changed and is unsaved

            message: {
                info: null,
                error: null,
                notice: null,
            },

            loginData: {
                loading: false,
                error: null,
                fieldErrors: {},
            },

		    signupData: {
		    	loading: false,
		    	error: null,
		    	fieldErrors: {},
		    },
			shareResult: {
				theirEmail: [''],
				yourEmail: null,
				personalMessage: null,
				error: {
					yourEmail: null,
					theirEmail: null,
					recaptcha: null
				},
			},
		    fieldErrors: {}, // currently only recording general questions' fields' errors. For goals analysis questions, they are unlikely to have an error, so we don't record them in fieldErrors, and just shoot users a global error message.

            saveProgressCallback: null,
            saveProgressCallbackParam: null,

            // generally, we don't keep the unanswered steps highlighted. Otherwise, it's gonna be annoying for users while they go through the questions. Then, this property is to store the steps which we want to highlight the unanswered questions. This happens after user click the "Jump to unanswered questions" button: jumpToFirstUnansweredQuestion(stepId)
            highlightedUnansweredSteps: [],
        }
    },
    methods: {

        _initData() {
            // this._initViewingStep();
            if (this.info && this.info.plan) {
                console.log(this.info.plan);
                this._initFormData();
                this._initGoalsAnalysisAnswersHaveChangedObj();
                Vue.nextTick(() => {
                    this._initStepsThatNeedsSave();
                });
            }

            

        },

        // _initViewingStep() {
        //     if(this.info) {
        //         // console.log(window.loadStep);
        //         // the above step needs to happen before the below one, because isStepViewable needs to use furthestViewableStep
        //         // determine the current viewing step
        //         if (this.isStepViewable(window.loadStep)) {
        //             // console.log('here');
        //             // there's a chance that the landing step comes from the URL param (set in planning-vue.html). That should be the highest priority. But before we set it to be the viewingStep, we will need to validate whether that's viewable.
        //             this.viewingStep = window.loadStep;
        //         } else if (this.info.plan && this.info.plan.currentStep) {
        //             // console.log('there');
        //             // second priority is to set the viewingStep to be the same as the plan's currentStep from db. This acts like "Continue filling the form", but the reality is that it will be very rare the code is executed inside this block, as it will only happen when user visits /planning/questionnaire directly without any URL params, which is very rare.
        //             this.viewingStep = this.info.plan.currentStep;
        //         } 
        //         // else if (this.firstInvalidStep) {
        //         //     // most likely the user will come here when they hit 'results' on 'your plan', but they have invalid steps, so results is not viewable. - hmm this doesn't really work, because the firstInvalidStep is probably hasn't been calculated when coming to this condition check, so it skips this block and go to the next check (this.viewingStep = '1'). It's ok I guess, this is only nice-to-have
        //         //     this.viewingStep = this.firstInvalidStep;
        //         // } 
        //         else {
        //             // otherwise, start with Step 1
        //             this.viewingStep = "1";
        //         }
        //     }
        // },

        /**
         * Initialise the form field values from the backend profile (answers in the plan entry). No action is required if user is not logged in (i.e. plan: null)
         */
        _initFormData() {
            if (this.info && this.info.plan) {
                // populate general questions fields
                this.formInputs.general = this.info.plan.generalQuestionAnswers;

                // const generalQuestionFieldHandles = Object.keys(this.info.plan.generalQuestionAnswers);
                // for (var i = 0; i < generalQuestionFieldHandles.length; i++) {
                // 	const handle = generalQuestionFieldHandles[i];
                // 	const answer = this.info.plan.generalQuestionAnswers[handle];
                // 	// console.log(handle);
                // 	// console.log(answer);
                // 	// console.log(document.getElementById('questionnaire-general-' + handle));
                // 	document.getElementById('questionnaire-general-' + handle).value = answer;
                // 	// console.log('#questionnaire [data-questiontype="general"][data-handle="' + handle + '"]');
                // 	// // console.log($form.find('[name="general[' + handle + ']"]').length);
                // 	// $('#questionnaire [data-questiontype="general"][data-handle="' + handle + '"]').val(answer);
                // }

                this.formInputs.goalsAnalysis.answers = this.info.plan.goalAnalysisAnswers;
                // this._initGoalsAnalysisAnswers();

                // this.formInputs.goalsAnalysis.goals = this.info.plan.goalAnalysisRanks;
                this._initGoalsAnalysisRanks();

                this._initOptionalGoals();
            }
        },

        // _initGoalsAnalysisAnswers() {
        // 	for (var i = 0; i < Object.keys(this.info.plan.goalAnalysisAnswers).length; i++) {
        // 		const questionId = Object.keys(this.info.plan.goalAnalysisAnswers)[i];
        // 		console.log(questionId);
        // 		const answerScore = this.info.plan.goalAnalysisAnswers[questionId];
        // 		console.log(answerScore);
        // 		this.formInputs.goalsAnalysis.answers[questionId] = answerScore;
        // 	}
        // },

        /**
         * Init goal analysis ranks cannot be simply done by `this.formInputs.goalsAnalysis.goals = this.info.plan.goalAnalysisRanks;` like answers, cuz it's causing issues that the sortedRankingList does not auto-update anymore. After investigation, we found out that the observer is gone after we do `this.formInputs.goalsAnalysis.goals = this.info.plan.goalAnalysisRanks;`, unlike the answers. It's a bit weird cuz we are basically doing the same thing compared to the answers. One guess is that each answer is a key:Int pair, but ranks have been changed from key:Int to key:Object pairs, and somehow Vue observer cannot follow that for some reasons. Anyway, the solution is to assign the rank and notes one by one like what we did in this function.
         * @return {[type]} [description]
         */
        _initGoalsAnalysisRanks() {
            for (var i = 0; i < Object.keys(this.info.plan.goalAnalysisRanks).length; i++) {
                const goalId = Object.keys(this.info.plan.goalAnalysisRanks)[i];
                const rankData = this.info.plan.goalAnalysisRanks[goalId];
                this.formInputs.goalsAnalysis.goals[goalId].rank = rankData.rank;
                this.formInputs.goalsAnalysis.goals[goalId].notes = rankData.notes;
            }
        },

        _initOptionalGoals() {
            for (var i = 0; i < Object.keys(this.info.plan.optionalGoals).length; i++) {
                const topicId = Object.keys(this.info.plan.optionalGoals)[i];
                const optionalGoal = this.info.plan.optionalGoals[topicId];
                this.formInputs.optionalGoals[topicId] = optionalGoal;
            }
        },

        _initGoalsAnalysisAnswersHaveChangedObj() {
            let result = {};
            for (var i = 0; i < this.info.questionnaire.steps.length; i++) {
                const step = this.info.questionnaire.steps[i];
                // result[step.stepId] = this.hasGoals(step.stepId) ? false : null;
                result[step.stepId] = false;
            }
            this.goalsAnalysisAnswersHaveChanged = result;
        },

        _initStepsThatNeedsSave() {
            // general question steps
            for (var i = 0; i < Object.keys(this.info.generalQuestionSettings).length; i++) {
                this.stepsThatNeedsSave[Object.keys(this.info.generalQuestionSettings)[i]] = false;
            }
            // goal analysis steps
            for (var i = 0; i < this.info.questionnaire.steps.length; i++) {
                this.stepsThatNeedsSave[this.info.questionnaire.steps[i].stepId] = false;
            }
        },

        validateStepId(stepId) {
            return this.availableStepIds && indexOf(stepId, this.availableStepIds) !== -1;
        },

        isStepViewable(stepId) {
            if (this.validateStepId(stepId)) {
                if (this.furthestViewableStep == 'completed') {
                    // if the plan is completed, then
                    if(stepId == 'completed') {
                        // if checking the result page
                        return this.isResultsViewable;
                    } else {
                        // if checking other steps
                        return true;
                    }
                    
                } else if (stepId == 'completed') {
                    // if the plan is not completed, the results page (stepId == completed) is not viewable
                    return false;
                } else {
                    return this.furthestViewableStep >= stepId;
                }
            }

            return false;
        },

        isStepCompleted(stepId) {
            if (this.isStepViewable(stepId)) {
                if (this.furthestViewableStep == 'completed') {
                    // if the plan is completed, then
                    return true;
                } else if (stepId == 'completed') {
                    // if the plan is not completed, the results page (stepId == completed) is not viewable
                    return false;
                } else {
                    // usually, the furthest viewable step is one step forward to the furthese completed step
                    return this.furthestViewableStep - 1 >= stepId;
                }
            }

            return false;
        },

        /**
         * Jump to a certain step. Try to save the progress if user is logged in. For not logged in users, jump directly.
         *
         * It validate the viewing step's form fields before doing so.
         * @param  {[type]} stepId [description]
         * @return {[type]}        [description]
         */
        jumpToStep(stepId) {
            if(this.isStepViewable(stepId) && stepId != this.viewingStep) {
                // so the messages do not appear anymore, which might not make sense
                this.message.info = null;
                this.message.error = null;

                // if (this.validateFormInViewingStep()) {
                    if (this.info.plan && this.hasUnsavedSteps()) {
                        this.saveProgressCallback = '_goToStep';
                        this.saveProgressCallbackParam = stepId;
                        this._saveProgress();
                    } else {
                        // if user hasn't had a profile yet, simply move on to the next step.
                        this._goToStep(stepId);
                    }
                // } else {
                //     alert("You can't continue! Please make sure you finish everything in this step.");
                // }
            } else if (this.isStepViewable(stepId) && stepId == this.viewingStep) {
                // clicking on a button skips to focus first question on this step
                this.$nextTick(() => $('#step' + stepId).find('input, textarea').first().focus());
            }
        },

        /**
         * (PRIVATE FUNCTION WARNING!)
         * This function is a private function (not supposed to be called directly in the template!)
         *
         * It is the 'action' which navigate users to the targeted step (of stepId). It is called by various other functions, such as _moveOnToNext(), jumpToStep(), etc.
         *
         * It purely check for whether the desired step is viewable (via Vue) and does not worry about form validation, assuming it is done previously by other steps.
         *
         * @param  {[type]} stepId [description]
         * @return {[type]}        [description]
         */
        _goToStep(stepId) {
            if (this.isStepViewable(stepId)) {
                // if(this.validateFormInViewingStep()) {
                this.viewingStep = stepId;
                
                scrollTo($('.site-header'), 400, function() {
                    $('.planning-vue__body').find('.sr-change-landing').first().focus();
                });
                // this.$nextTick(() => $('.planning-nav__step.current .planning-nav__step-btn').focus());

                // } else {
                // 	alert("You can't continue! Please make sure you finish everything in this step.");
                // }
            }
        },
        _reload(params) {
            location.reload();
        },

        /**
         * The action that move the step to next one.
         *
         * (PRIVATE FUNCTION WARNING!)
         * CAUTION! This function is a private function (not supposed to be called directly in the template!). It is used by continueToNextStep(). It will force update the "furthestViewableStep", which is not always supposed to do. To safely "continue" to next step, please use "continueToNextStep()"
         * @return {[type]} [description]
         */
        _moveOnToNext() {
            if (this.nextStepId) {
                if (this._canIncreaseStepCapOnContinue()) {
                    this.furthestViewableStep = this.nextStepId;
                }

                this._goToStep(this.nextStepId);
            }
        },

        /**
         * Indicate whether we can increase the furthestViewableStep if we hit continue - this is only true if there is a next step and next step is greater than the current furthest viewable step.
         * @return {[type]} [description]
         */
        _canIncreaseStepCapOnContinue() {
            return this.nextStepId && this.furthestViewableStep < this.nextStepId;
        },

        // It's weird that we can't just use "continue" as the function name as it results in errors. It could be reserved?
        continueToNextStep() {

            if (this.validateFormInViewingStep()) {
                // so the messages do not appear anymore, which might not make sense
                this.message.info = null;
                this.message.error = null;

                if (this.info.plan && this.hasUnsavedSteps()) {
                    this.saveProgressCallback = "_moveOnToNext";
                    this._saveProgress();
                } else {
                    // if user hasn't had a profile yet, simply move on to the next step.
                    this._moveOnToNext();
                }
            } else {
                alert("You can't continue! Please make sure you finish everything in this step.");
            }
        },

        save() {
            this.saveProgressCallback = null;
            this.saveProgressCallbackParam = null;
            this._saveProgress();
        },

        /*
        Deprecated. We no longer have "Save and Exit" button
         */
        quitApp() {
            // TODO: ask users to login / create an account before continue
            // if(this.validateFormInViewingStep()) {
            this.saveProgressCallback = '_closeWindow';
            this._saveProgress();
            // } else {
            // 	alert("You can't continue! Please make sure you finish everything in this step.");
            // }
        },

        _closeWindow() {
            console.log('closing');
            this.loggingOut = true;
            this.message.notice = "Logging out...";

            window.location.href = '/logout';
            // // users should click the browser's quit button instead of scripts to trigger it. The modern browsers have blocked this behaviour. What we can do is to replace the current window by some other URL (e.g. Go back to the "Your plan" page)
            // window.home();
            // window.close('','_parent',''); // buggy and not working since browsers now prevent it
        },

        _onBeforeSaveProgress() {
            this.saving = true;
            this.fieldErrors = {};
            this.message.notice = "Saving your progress...";
            this.message.info = null;
            this.message.error = null;
        },

        /**
         * (PRIVATE FUNCTION WARNING!)
         * This function is used as a private function (can't be used directly by the template), assuming we know the current user has a profile (plan entry) in the backend. It is called by any step navigation for a logged-in user (and also when save & exit)
         *
         * Save the current progress and update info.plan when successfully saved in the backend
         * @param {string} [callback] callback function in current scope (i.e. this)
         * @return {[type]} [description]
         */
        _saveProgress() {
            // const postData = {
            //     step: this.viewingStep,
            //     // [window.csrfTokenName]: window.csrfTokenValue,
            // };

            let postData = $("#questionnaire").serialize();
            // console.log(postData);
            if (this.saveProgressCallback == '_moveOnToNext' && this._canIncreaseStepCapOnContinue()) {
                postData += "&continue=1"; // tell the backend that we are trying to save & go to the next step, so the server know it needs to add 1 to currentStep of the plan entry
            }
			if(!this.info.plan) {
				postData += "&returnPlan=1"; // if we does not have plan details yet, tell the backend to return the plan object, so we can update it on callback
			}

            // reset things before saving
            this._onBeforeSaveProgress();

            axios.post('/actions/yodhub-module/planning/save-progress', postData)
                .then(response => {
                    // any 2xx responses
                    // console.log(response);

                    if (response.data.success) {
                        if(response.data.plan) {
			    		this.info.plan = response.data.plan; // update the plan details. This only happens if the user did not login when page loads
			    	}
                        // this.message.info = "Progress saved.";

                        this._markAllStepsSaved(); // mark all steps saved

                        if(this.saveProgressCallback) {
                            this[this.saveProgressCallback](this.saveProgressCallbackParam); // call the callback
                        }
                        
                        this.showSavedScreen = true;
                        this.message.notice = "Saved!"
                        setTimeout(() => this.showSavedScreen = false, 1500);

                    } else {
                        if (response.data.errors) {
                            this.fieldErrors = response.data.errors;
                            this.message.error = "Failed to save progress. Please  try again.";
                        }
                    }

                    this.saveProgressCallback = null;
                    this.saveProgressCallbackParam = null;
                })
                .catch(error => {
                    if (error.response) {
                        // The request was made and the server responded with a status code
                        // that falls out of the range of 2xx
                        // console.log(error.response.data);
                        // console.log(error.response.status);
                        // console.log(error.response.headers);

                        if (error.response.status == 403) {
                            console.log('Not logged in!');
                            // // Trigger a pop up of the login screen, since users who come here will be login session expired users. After successful login, we should try again with whatever the callback is
                            // this.saveProgressCallback = callback;
                            // this.saveProgressCallbackParam = callbackParam;
                            window.showPopup('save-progress');
                            // do not reset saveProgressCallback and saveProgressCallbackParam in this case, because we will need them later.
                        } else if (error.response.status == 400) {
                            this.message.error = 'We could not submit this request because of your session has expired. Please go back to <a class="text-link" href="/planning/dashboard">your profile</a> to continue working on your saved plan.';
                        } else {
                            if (error.response.data.error) {
                                this.message.error = error.response.data.error;
                            }
                            this.saveProgressCallback = null;
                            this.saveProgressCallbackParam = null;
                        }
                        return;
                    }
                    this.message.error = "Unknown error occurs. Please try again.";

                    this.saveProgressCallback = null;
                    this.saveProgressCallbackParam = null;
                })
                .finally(() => {
                    this.saving = false;
                })
        },

        _markAllStepsSaved() {
            for (var i = 0; i < Object.keys(this.stepsThatNeedsSave).length; i++) {
                const stepId = Object.keys(this.stepsThatNeedsSave)[i];
                this.stepsThatNeedsSave[stepId] = false;
            }
        },

        /**
         * Indicate users can leave current step or not. Normally this will go through form validation of the current step.
         */
        validateFormInViewingStep() {
            return this.validateForm(this.viewingStep);
        },

        validateForm (stepId) {
            if (this.info && stepId) {
                if(stepId == 'completed') {
                    return true;
                }if (this.getGoalsAnalysisQuestionnaire(stepId)) {
                    // goal analysis questions: firstly it needs to hasGoals, and also all the questions should have been answered.
                    if (this.hasGoals(stepId) && this.validateGoalsAnalysisQuestions(stepId)) {
                        return true;
                    }
                } else {
                    // general questions: all required fields should not be empty

                    // // Deprecated jQuery code
                    // const $viewingStepRequiredFields = $('.questionnaire-step[data-stepid="' + stepId + '"]').find('.form-input[required]');
                    // let valid = true;
                    // $viewingStepRequiredFields.each(function() {
                    //  const val = $(this).val();
                    //  if(typeof val === 'undefined' || val === null || (typeof val === 'string' && val.trim() == '')) {
                    //      valid = false;
                    //  }
                    // })
                    // return valid;

                    return this.validateGeneralQuestions(stepId);
                }
            }
            return false;
        },

        /**
         * When "Copy these activities to other days" is clicked, we update the sourceDay attribute and pop up a modal to ask users to select the target days
         * @param  {[string]} group [description]
         * @return {[type]}       [description]
         */
        copyActivities(group) {
            this.sourceDay = group;
            $('#activities-days-selector-modal').modal();
        },

        /**
         * When the 'Apply' button in the day selector modal is clicked, we copy the activities of the source day into other selected days' activity fields.
         * @return {[type]} [description]
         */
        applyCopyActivities() {
            if (this.sourceDay && this.daysSelector.length > 0) {
                // console.log(this.sourceDay, this.daysSelector);
                const whatIDoSource = this.formInputs.general['whatIDo' + this.sourceDay];
                const supportFromSource = this.formInputs.general['supportFrom' + this.sourceDay];

                for (var i = 0; i < this.daysSelector.length; i++) {
                    var day = this.daysSelector[i]
                    this.formInputs.general['whatIDo' + day] = whatIDoSource;
                    this.formInputs.general['supportFrom' + day] = supportFromSource;
                }

                $('#activities-days-selector-modal').modal('hide');

                // focus on next blank textarea
                $('#activities-days-selector-modal').on('hidden.bs.modal', function () {
                    var emptyAreas = $('#step2 textarea').filter(function (index, element) {
                        return $.trim($(element).val()) === ''; // element .val() is '' after trimming white spaces = true
                    });
                    emptyAreas.delay(1000).first().focus();
                });

            }
        },

        /**
         * Select all days in the day selector (Step 2)
         * @return {[type]} [description]
         */
        selectAllDays() {
            this.daysSelector = [
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday',
                'Sunday',
            ];
        },

        /**
         * Deselect all days in the day selector (Step 2)
         * @return {[type]} [description]
         */
        deselectAllDays() {
            this.daysSelector = [];
        },

        /**
         * Find Goals Analysis questionnaire for the given step
         */
        getGoalsAnalysisQuestionnaire(stepId) {
            if (this.info) {
                return this.info.questionnaire.steps.find(element => element.stepId == stepId);
            }
        },

        suggestGoals(stepId) {
            let result = this.calculateGoalsRanking(stepId);
            // console.log(result);
            if (result) {
                for (var i = 0; i < Object.keys(result).length; i++) {
                    const goalId = Object.keys(result)[i];
                    this.formInputs.goalsAnalysis.goals[goalId].rank = result[goalId];
                }
                this.goalsAnalysisAnswersHaveChanged[stepId] = false; // reset the 'answers have changed' indicator, so we can capture the changes again.
            }
        },

        calculateGoalsRanking(stepId) {
            if (this.autoScores && this.autoScores[stepId]) {
                const scores = this.autoScores[stepId];
                let goalsArr = [];
                for (var i = 0; i < Object.keys(scores).length; i++) {
                    const goalId = Object.keys(scores)[i];
                    const scoreObj = scores[goalId];
                    const goalScore = scoreObj.score / scoreObj.maxScore;
                    goalsArr.push({
                        goalId: goalId,
                        score: goalScore,
                    })
                }

                // sort the array by score, and also by the order in the backend. The higher ranked item should be placed first.
                goalsArr.sort((a, b) => {
                    if (a.score < b.score) {
                        return 1;
                    } else if (a.score > b.score) {
                        return -1;
                    } else {
                        // if equal, sort by the order of the goals array returned from the backend (the first one in the backend will have higher rank)
                        const questionnaire = this.findQuestionnaire(stepId);
                        const goals = questionnaire.topic.goals;
                        const indexA = goals.findIndex((element) => element.id == a.goalId);
                        const indexB = goals.findIndex((element) => element.id == b.goalId);
                        // assuming indexA and indexB won't be -1: not found, and obviously, indexA can't be equal to indexB
                        if (indexA < indexB) {
                            return -1;
                        } else {
                            return 1;
                        }
                    }
                });

                // give the results their rankings, and form the result in the key => value format, so we can easily assign them to the formInputs
                let result = {};
                for (var i = 0; i < goalsArr.length; i++) {
                    let goal = goalsArr[i];
                    // result[goal.goalId] = {
                    // 	rank: i + 1,
                    // 	notes: null,
                    // }
                    result[goal.goalId] = i + 1;
                }

                return result;
            }
        },

        findQuestionnaire(stepId) {
            if (this.info) {
                return this.info.questionnaire.steps.find(element => element.stepId == stepId);
            }
        },

        /**
         * Validate whether this step (goals analysis step) questions have all been completed
         * @param  {[type]} stepId [description]
         * @return {bool|undefined}        bool if this step is a goals analysis step. undefined if it's not.
         */
        validateGoalsAnalysisQuestions(stepId) {
            // const questionnaire = this.findQuestionnaire(stepId);
            // if(questionnaire) {
            // 	const questions = questionnaire.topic.questions;
            // 	for (var i = 0; i < Object.keys(questions).length; i++) {
            // 		const questionType = Object.keys(questions)[i];
            // 		for (var j = 0; j < questions[questionType].length; j++) {
            // 			const question = questions[questionType][j];
            // 			// console.log(question.id, this.formInputs.goalsAnalysis.answers[question.id]);
            // 			if(typeof this.formInputs.goalsAnalysis.answers[question.id] === 'undefined' || this.formInputs.goalsAnalysis.answers[question.id] === null) {
            // 				return false;
            // 			}
            // 		}
            // 	}
            // 	return true;
            // }

            // don't validate the results step.
            if (stepId == 'completed') {
                return true;
            }

            if (this.goalsAnalysisAnswers && this.goalsAnalysisAnswers.hasOwnProperty(stepId)) {
                const questionIds = Object.keys(this.goalsAnalysisAnswers[stepId]);
                if (!questionIds.length) return false;

                for (var i = 0; i < questionIds.length; i++) {
                    const questionId = questionIds[i];
                    const answer = this.goalsAnalysisAnswers[stepId][questionId];
                    if (typeof answer === 'undefined' || answer === null) {
                        return false;
                    }
                }
                return true;
            }
        },

        validateGoalsAnalysisSingleQuestion(stepId, questionId) {
            return this.goalsAnalysisAnswers && this.goalsAnalysisAnswers.hasOwnProperty(stepId) && this.goalsAnalysisAnswers[stepId].hasOwnProperty(questionId) && this.goalsAnalysisAnswers[stepId][questionId] !== null && typeof this.goalsAnalysisAnswers[stepId][questionId] !== 'undefined';
        },

        validateGeneralQuestions(stepId) {
            if (this.info && this.info.generalQuestionSettings.hasOwnProperty(stepId)) {
                for (var i = 0; i < this.info.generalQuestionSettings[stepId].length; i++) {
                    const questionSettings = this.info.generalQuestionSettings[stepId][i];
                    if (questionSettings.hasOwnProperty('fields')) {
                        // step 2 has special structure, that 'fields' settings are nested
                        for (var j = 0; j < questionSettings.fields.length; j++) {
                            if (questionSettings.fields[j].hasOwnProperty('required') && questionSettings.fields[j].required) {
                                if (!this.formInputs.general[questionSettings.fields[j].handle]) {
                                    return false;
                                }
                            }
                        }
                    } else {
                        if (questionSettings.hasOwnProperty('required') && questionSettings.required) {
                            if (!this.formInputs.general[questionSettings.handle]) {
                                return false;
                            }
                        }
                    }
                }
                return true;

            }
        },

        calculateScores(stepId) {
            if (this.validateGoalsAnalysisQuestions(stepId)) {
                const questionnaire = this.findQuestionnaire(stepId);
                if (questionnaire) {
                    let result = {};
                    const questions = questionnaire.topic.questions;
                    for (var i = 0; i < questions.personalImportance.length; i++) {
                        const question = questions.personalImportance[i];
                        if (!result.hasOwnProperty(question.goalId)) {
                            result[question.goalId] = {
                                score: 0,
                                maxScore: 0,
                            }
                        }

                        result[question.goalId].score += parseInt(this.formInputs.goalsAnalysis.answers[question.id]);
                        result[question.goalId].maxScore += parseInt(this.maxScorePerQuestionPersonalImportance);
                    }
                    return result;
                }
            }
        },

        /**
         * This can be achieved by checking: sortedRankingList[stepId].length
         *
         * indicate if current step has any goals - to help show / hide the goals section
         * @param  {[type]}  stepId [description]
         * @return {Boolean}        [description]
         */
        hasGoals(stepId) {
            if (this.validateGoalsAnalysisQuestions(stepId)) {
                const questionnaire = this.findQuestionnaire(stepId);
                if (questionnaire) {
                    for (var i = 0; i < questionnaire.topic.goals.length; i++) {
                        let goal = questionnaire.topic.goals[i];
                        let goalInput = this.formInputs.goalsAnalysis.goals[goal.id];
                        // rank can be -1 (disabled/inactive) or positive integers
                        // console.log(goalInput);
                        if (goalInput && goalInput.rank) {
                            return true;
                        }
                    }
                }
            }
            return false;
        },

        calculateNextAvailableRank(stepId) {
            const availableGoals = this.getAvailableGoals(stepId);
            if (availableGoals) {
                let currentHighestNumber = 0;
                for (var i = 0; i < availableGoals.length; i++) {
                    const goal = availableGoals[i];
                    if (goal.rank > currentHighestNumber) {
                        currentHighestNumber = goal.rank;
                    }
                }
                return currentHighestNumber + 1;
            }
        },

        /**
         * Enable / disabled the goal - triggered by the checkbox change event. Users have the ability to do so since they can mark a goal as not relevant to them.
         * @param  {[type]} goalId [description]
         * @return {[type]}        [description]
         */
        toggleGoal(goalId, stepId, event) {
            const oldRank = this.formInputs.goalsAnalysis.goals[goalId].rank;

            if (oldRank) {
                // enabling
                if (oldRank === -1) {
                    const currentActiveGoals = this.activeRankingList[stepId].slice(0); // get active goals in current step and make a clone
                    let currentHighestNumber = 0;
                    for (var i = 0; i < currentActiveGoals.length; i++) {
                        const goal = currentActiveGoals[i];
                        if (goal.rank > currentHighestNumber) {
                            currentHighestNumber = goal.rank;
                        }
                    }
                    this.formInputs.goalsAnalysis.goals[goalId].rank = currentHighestNumber + 1;
                } else { // disabling
                    this.formInputs.goalsAnalysis.goals[goalId].rank = -1;
                }
            }

            // console.log(this.formInputs.goalsAnalysis.goals[goalId].rank);

            // Re-assign the rank value to the available goals in current step, based on the order of the available goal array.
            // Vue.nextTick(() => {
            // assuming when the toggleGoal function is triggered, we can always find an item of stepId in activeRankingList. So no error check is needed
            const activeGoals = this.activeRankingList[stepId].slice(0); // get updated active goals in current step and make a clone
            // console.log(activeGoals);
            for (var i = 0; i < activeGoals.length; i++) {
                const goal = activeGoals[i];
                this.formInputs.goalsAnalysis.goals[goal.id].rank = i + 1;
            }
            // });

        },

        fieldHasError(handle) {
            return this.fieldErrors.hasOwnProperty(handle);
        },

        signupFieldHasError(handle) {
            return this.signupData.fieldErrors.hasOwnProperty(handle);
        },

        /**
         * Deprecated. DO NOT USE
         * @param  {[type]} postData [description]
         * @param  {[type]} action   [description]
         * @return {[type]}          [description]
         */
        login(postData, action) {
            this.loginData.loading = true;
            this.message.notice = "Logging in...";
            this.loginData.error = null;

            axios.post("/" + action, postData)
                .then(response => {
                    // any 2xx responses
                    // console.log(response);

                    if (response.data.success) {
                        if (response.data.csrfTokenValue) {
                            this.csrfTokenValue = response.data.csrfTokenValue;
                        }

                        window.hidePopup('signin');

                        Vue.nextTick(function () {
                            planningVue._saveProgress()
                        });

                    } else if (response.data.error) {
                        this.loginData.error = response.data.error;
                    }

                    // if(response.data.success) {
                    // 	// this.info.plan = response.data.plan; // update the plan details
                    // 	this.message.info = "Progress saved.";

                    // 	this[callback] (callbackParam); // call the callback
                    // } else {
                    // 	if(response.data.errors) {
                    // 		this.fieldErrors = response.data.errors;
                    // 		this.message.error = "Failed to save progress. Please review your form and try again.";
                    // 	}
                    // }
                })
                .catch(error => {
                    if (error.response) {
                        // The request was made and the server responded with a status code
                        // that falls out of the range of 2xx
                        // console.log(error.response.data);
                        // console.log(error.response.status);
                        // console.log(error.response.headers);

                        // console.log(error.response);
                        this.loginData.error = 'We could not log you in due to technical issues. Please go back to <a href="/planning/dashboard">Your plan</a> to continue working on your plan.'
                        return;
                    }

                })
                .finally(() => {
                    this.loginData.loading = false;
                })
        },

        signup(postData, action) {
            this.signupData.loading = true;
            this.message.notice = "Signing you up...";
            this.signupData.error = null;
            this.signupData.fieldErrors = {};

            // handle reCaptcha
            var response = grecaptcha.getResponse(window.signupFormCaptchaWidgetId);
            // console.log(response);
            if (response.length === 0) {
                //reCaptcha not verified
                this.signupData.error = "Invalid reCaptcha response!";
                this.signupData.loading = false;
                this.signupData.fieldErrors.reCaptcha = ["Please complete the reCaptcha before proceeding."];
                return false;
            }

            axios.post("/" + action, postData)
                .then(response => {
                    // any 2xx responses
                    // console.log(response);

                    if (response.data.success) {
                        this.signupData.loading = true;
						axios.get("/actions/users/session-info")
							.then(response => {
								// any 2xx responses
								// console.log(response);
                                if (response.data.csrfTokenValue) {
                                    this.csrfTokenValue = response.data.csrfTokenValue;

                                    window.hidePopup('save-progress');

                                    Vue.nextTick(function () {
                                        // TODO: by signing up and auto log users in, the CRSF token has been changed. It needs to be updated. However, the saveUser() controller action does not return it like login action, so we need to figure something else out for it.
                                        planningVue.saveProgressCallback = '_reload';
                                        planningVue.saveProgressCallbackParam = 1; 
                                        
                                        // if(planningVue.viewingStep)
                                        //     var currentStep = planningVue.viewingStep;
                                        // else    
                                        //     var currentStep = 1;
                                        
                                        // for (var i = 1; i <= currentStep; i++) {
                                        //     planningVue.stepsThatNeedsSave[i] = true;
                                        // }
        
                                        planningVue._saveProgress();
                                         
                                    });
                                } else if (response.data.isGuest) {
    								this.signupData.error = 'We have successfully signed you up but we could not sign you in. Please try again or proceed without saving.'
    							}
    						})
    						.catch(error => {
    							if (error.response) {
    							  // The request was made and the server responded with a status code
    							  // that falls out of the range of 2xx
    							  // console.log(error.response.data);
    							  // console.log(error.response.status);
    							  // console.log(error.response.headers);

    							  // console.log(error.response);
    							  this.signupData.error = 'We have successfully signed you up but we could not sign you in. Please try again, or proceed without saving.'
    							  return;
    							}

    						})
    						.finally(() => {
    							this.signupData.loading = false;
    						})
                    } else if (response.data.error) {
                        this.signupData.error = response.data.error;
                    } else if (response.data.errors) {
                        // when we have error from the user element fields
                        this.signupData.error = "Please review your form for the errors.";
                        this.signupData.fieldErrors = response.data.errors;}

                })
                .catch(error => {
                    if (error.response) {
                        // The request was made and the server responded with a status code
                        // that falls out of the range of 2xx
                        // console.log(error.response.data);
                        // console.log(error.response.status);
                        // console.log(error.response.headers);

                        // console.log(error.response);
                        this.signupData.error = 'We could not sign you up due to technical issues. Please try again.'
                        return;
                    }

                })
                .finally(() => {
                    this.signupData.loading = false;
                })
        },

        isGeneralQuestionStep(stepId) {
            if (this.info) {
                return this.info.generalQuestionSettings.hasOwnProperty(stepId);
            }
        },

        scrollToResultsStep(stepId) {
  			const $target = $('#results-step' + stepId);
  			scrollTo($target, 400, function() {
                $target.find('.sr-change-landing').focus();
            });
  		},

  		backToTop() {
  			scrollTo($('.planning-results'), 400, $('.planning-results').find('.sr-change-landing').first().focus());
  		},

		/** * Generate downloadable PDF
         * @param  {[type]} postData [description]
         * @param  {[type]} action   [description]
         * @return {[type]}          [description]
         */
        downloadPdf() {
            $('#planning-results-form').attr('action', '/actions/yodhub-module/planning/pdf-download');
            $('#planning-results-form').submit();
        },

        /**
         * Generate downloadable PDF and email
         * @param  {[type]} postData [description]
         * @param  {[type]} action   [description]
         * @return {[type]}          [description]
         */
        emailPdf() {
            window.showPopup('share-results');
        },

        /**
         * Generate downloadable PDF and email
         * @return {[type]}          [description]
         */
        emailPdfShare() {

            this.shareResult.error.theirEmail = null;
            this.shareResult.error.yourEmail = null;
            this.shareResult.error.recaptcha = null;

            var theirEmailError = true;
            var formError = false;
            //validation
            for (var i = 0; i < this.shareResult.theirEmail.length; i++) {
                if (this.shareResult.theirEmail[i] !== '' && validateEmail(this.shareResult.theirEmail[i])) {
                    theirEmailError = false;
                }
            }
            if (theirEmailError) {
                formError = true;
                $('.theirEmailValue').addClass('is-invalid');
                this.shareResult.error.theirEmail = 'Please enter at least one valid email address!';
            }
            if (this.shareResult.yourEmail === null) {
                this.shareResult.error.yourEmail = "Your email is required!";
                $('#yourEmail').addClass('is-invalid');
                formError = true;
            } else {
                if (!validateEmail(this.shareResult.yourEmail)) {
                    this.shareResult.error.yourEmail = "Please enter a valid email";
                    $('#yourEmail').addClass('is-invalid');
                    formError = true;
                }
            }
            var response = grecaptcha.getResponse(window.emailShareFormCaptchaWidgetId);
            // console.log(response);

            if (response.length === 0) {
                //reCaptcha not verified
                this.shareResult.error.recaptcha = "Please verify you are human!";
                formError = true;
            }

            if (formError) {
                return false;
            }
            //submit form
            window.hidePopup('share-results');
            $('#sharePdf').val(JSON.stringify(this.shareResult));
            $('#shareReCaptcha').val(response);
            $('#planning-results-form').attr('action', '/actions/yodhub-module/planning/pdf-email');
            $('#planning-results-form').submit();
        },

        addTheirEmail() {
            this.shareResult.theirEmail.push("");
        },

        hasUnsavedSteps() {
            for (var i = 0; i < Object.keys(this.stepsThatNeedsSave).length; i++) {
                const stepId = Object.keys(this.stepsThatNeedsSave)[i];
                // console.log(stepId, this.stepsThatNeedsSave[stepId]);
                if(this.stepsThatNeedsSave[stepId]) {
                    return true;
                }
            }

            return false;
        },

        jumpToFirstUnansweredQuestion(stepId) {
            let unanswered = false;
            // Not very DRY cuz it's kinda duplicate with validateGoalsAnalysisQuestions(), but for the sake of deadline...
            if (this.goalsAnalysisAnswers && this.goalsAnalysisAnswers.hasOwnProperty(stepId)) {
                const questionIds = Object.keys(this.goalsAnalysisAnswers[stepId]);
                if (!questionIds.length) {
                    unanswered = true;
                }

                for (var i = 0; i < questionIds.length; i++) {
                    const questionId = questionIds[i];
                    const answer = this.goalsAnalysisAnswers[stepId][questionId];
                    if (typeof answer === 'undefined' || answer === null) {
                        unanswered = questionId;
                        break;
                    }
                }
            }
            // until now, 'unanswered' can be:
            // 1. false: which means all answered.
            // 2. true: which means has unanswered but not found, in this case we scrollToTop
            // 3. questionId: indicate the first unanswered question's ID, we scroll to it.

            if(unanswered) {
                if(this.highlightedUnansweredSteps.indexOf(stepId) === -1) {
                    this.highlightedUnansweredSteps.push(stepId);
                }
                // this.stepsWithUnansweredHighlighted[stepId] = true; // highlight the unanswered questions in this step

                if(unanswered === true) {
                    scrollTo($('#questionnaire'), 500, function() {
                        $('.planning-vue__body').find('.sr-change-landing').first().focus();
                    });
                } else {
                    scrollTo($('#goals-analysis-question-' + unanswered), 500, function() {
                        $('#goals-analysis-question-' + unanswered).find('.field-label').first().focus();
                    });
                }
            }
        },

    },
    computed: {
        /**
         * WARNING - Redundant, not in use
         *
         * Get the current viewing step's questionnaire
         * @return {[type]} [description]
         */
        viewingStepGoalsAnalysisQuestionnaire() {
            this.findQuestionnaire(this.viewingStep);
        },
        stepDetails () {
  			if(this.info) {
  				let result = {};
  				for (var i = 0; i < this.info.stepDetails.length; i++) {
  					const step = this.info.stepDetails[i];
  					result[step.stepId] = step;
  				}
  				return result;
  			}
  		},viewingStepDetails() {
            if (this.stepDetails) {
                if (this.viewingStep == 'completed') {
                    return null;
                } else {
                    return this.stepDetails[this.viewingStep];
                }
            }
        },
        availableStepIds() {
            if (this.info) {
                const stepIds = this.info.stepDetails.map(element => element.stepId);
                stepIds.push('completed');
                return stepIds;
            }
        },
        prevStepId() {
            if (this.viewingStep && this.availableStepIds) {
                const indexOfViewingStep = indexOf(this.viewingStep, this.availableStepIds);
                if (indexOfViewingStep !== -1 && typeof this.availableStepIds[indexOfViewingStep - 1] !== 'undefined') {
                    return this.availableStepIds[indexOfViewingStep - 1];
                }
            }
        },
        nextStepId() {
            if (this.viewingStep && this.availableStepIds) {
                const indexOfViewingStep = indexOf(this.viewingStep, this.availableStepIds);

                if (indexOfViewingStep !== -1 && typeof this.availableStepIds[indexOfViewingStep + 1] !== 'undefined') {
                    return this.availableStepIds[indexOfViewingStep + 1];
                }
            }
        },

        /**
         * The scores calculated automatically by the users' selected
         * @return {[type]} [description]
         */
        autoScores() {
            if (this.info) {
                let result = {};
                for (var i = 0; i < this.info.questionnaire.steps.length; i++) {
                    const step = this.info.questionnaire.steps[i];
                    result[step.stepId] = this.calculateScores(step.stepId);
                }
                return result;
            }
        },

        /**
         * Based on the scoreDefinition, auto calculate what's the max score every question of personal importance can have (based on the questionnaire doc Version 1, the result is 5, but it's better to calculate it instead of hard code it.)
         * @return {[type]} [description]
         */
        maxScorePerQuestionPersonalImportance() {
            if (this.info) {
                let maxScore = 0;
                for (var i = 0; i < this.info.questionnaire.scoreDefinitions.personalImportance.length; i++) {
                    const scoreItem = this.info.questionnaire.scoreDefinitions.personalImportance[i]
                    if (scoreItem.score > maxScore) {
                        maxScore = scoreItem.score;
                    }
                }
                return maxScore;
            }
        },

        sortedRankingList() {
            if (this.info) {
                let result = {};
                for (var i = 0; i < this.info.questionnaire.steps.length; i++) {
                    const questionnaire = this.info.questionnaire.steps[i];
                    let goals = [];
                    for (var j = 0; j < questionnaire.topic.goals.length; j++) {
                        const goal = questionnaire.topic.goals[j];
                        const goalInput = this.formInputs.goalsAnalysis.goals[goal.id];
                        // rank can be -1 (disabled/inactive) or positive integers
                        if (goalInput && goalInput.rank) {
                            goals.push({
                                id: goal.id,
                                rank: goalInput.rank,
                                notes: goalInput.notes,
                                title: goal.title,
                            })
                        }
                    }

                    goals.sort((a, b) => {
                        if (a.rank !== -1 && b.rank !== -1) {
                            if (a.rank < b.rank) {
                                return -1;
                            } else if (a.rank > b.rank) {
                                return 1;
                            } else {
                                return 0;
                            }
                        } else { // if a or b has ranking === -1 (inactive)
                            if (a.rank === b.rank) {
                                return 0;
                            } else if (a.rank === -1) {
                                return 1;
                            } else {
                                return -1;
                            }
                        }
                    });

                    result[questionnaire.stepId] = goals;
                }

                return result;
            }
        },

        activeRankingList() {
            if (this.sortedRankingList) {
                let result = {};
                for (var i = 0; i < Object.keys(this.sortedRankingList).length; i++) {
                    const stepId = Object.keys(this.sortedRankingList)[i];
                    const goals = this.sortedRankingList[stepId];
                    let activeGoals = [];
                    for (var j = 0; j < goals.length; j++) {
                        const goal = goals[j];
                        if (goals[j].rank && goals[j].rank !== -1) {
                            activeGoals.push(goal);
                        }
                    }
                    result[stepId] = activeGoals;
                }

                return result;
            }
        },

        /**
         * split formInputs.goalsAnalysis into associative array by stepId - this is useful for detecting whether the answers for a step has changed.
         * @return {[type]} [description]
         */
        goalsAnalysisAnswers() {
            if (this.formInputsByStep) {
                let result = {};
                for (var i = 0; i < this.info.questionnaire.steps.length; i++) {
                    const questionnaire = this.info.questionnaire.steps[i];

                    result[questionnaire.stepId] = this.formInputsByStep[questionnaire.stepId].answers;
                }
                return result;
            }
        },

        formInputsByStep() {
            if (this.info) {
                let result = {};

                // general questions
                for (var i = 0; i < Object.keys(this.info.generalQuestionSettings).length; i++) {
                    const generalStepId = Object.keys(this.info.generalQuestionSettings)[i];
                    result[generalStepId] = {};
                    for (var j = 0; j < this.info.generalQuestionSettings[generalStepId].length; j++) {
                        if (this.info.generalQuestionSettings[generalStepId][j].hasOwnProperty('fields')) {
                            for (var k = 0; k < this.info.generalQuestionSettings[generalStepId][j].fields.length; k++) {
                                const fieldSettings = this.info.generalQuestionSettings[generalStepId][j].fields[k];
                                result[generalStepId][fieldSettings.handle] = this.formInputs.general[fieldSettings.handle];
                            }
                        } else {
                            const fieldSettings = this.info.generalQuestionSettings[generalStepId][j];
                            result[generalStepId][fieldSettings.handle] = this.formInputs.general[fieldSettings.handle];
                        }

                    }
                }

                // goal analysis questions
                for (var i = 0; i < this.info.questionnaire.steps.length; i++) {
                    const stepObj = this.info.questionnaire.steps[i];
                    result[stepObj.stepId] = {
                        answers: {},
                        goals: {},
                        optionalGoal: this.formInputs.optionalGoals[stepObj.topic.id],
                    };
                    for (var j = 0; j < Object.keys(stepObj.topic.questions).length; j++) {
                        const type = Object.keys(stepObj.topic.questions)[j];
                        for (var k = 0; k < stepObj.topic.questions[type].length; k++) {
                            const question = stepObj.topic.questions[type][k];
                            const answer = this.formInputs.goalsAnalysis.answers[question.id];
                            result[stepObj.stepId].answers[question.id] = answer;
                        }
                    }

                    for (var j = 0; j < stepObj.topic.goals.length; j++) {
                        const goal = stepObj.topic.goals[j];
                        result[stepObj.stepId].goals[goal.id] = {
                            rank: this.formInputs.goalsAnalysis.goals[goal.id].rank,
                            notes: this.formInputs.goalsAnalysis.goals[goal.id].notes,
                        };
                    }
                }

                return result;
            }
        },

        stepValidationStatus() {
            if(this.info) {
                let result = {};
                for (var i = 0; i < this.availableStepIds.length; i++) {
                    const stepId = this.availableStepIds[i]
                    if(stepId != 'completed') {
                        result[stepId] = this.validateForm(stepId);
                    }
                }
                return result;
            }
        },

        firstInvalidStep() {
            if(this.stepValidationStatus) {
                for (var i = 0; i < Object.keys(this.stepValidationStatus).length; i++) {
                    const stepId = Object.keys(this.stepValidationStatus)[i];
                    if(!this.stepValidationStatus[stepId]) {
                        return stepId;
                    }
                }
                return null; // indicates all steps are valid
            }
        },

        isResultsViewable() {
            return this.firstInvalidStep === null;
        },

    //     hasUnsavedSteps () {
    //         for (var i = 0; i < Object.keys(this.stepsThatNeedsSave).length; i++) {
    //             const stepId = Object.keys(this.stepsThatNeedsSave)[i];
    //             // console.log(stepId, this.stepsThatNeedsSave[stepId]);
    //             if(this.stepsThatNeedsSave[stepId]) {
    //                 return true;
    //             }
    //         }

  		// 	return false;
  		// },

    },

    watch: {
        viewingStep(newVal) {
            updateUrlParams(newVal);
        },
        initialising(newVal) {
            if (!newVal) {
                Vue.nextTick(() => {
                    this._initData();
                });
            }
        },
        info: {
            handler: (newVal) => {

            },
            deep: true,
        },
        activeRankingList: {
            handler: function (newVal, oldVal) {

                // listen for goal ranking swapping and handle it. Usually this only happens on current step, and also the lists' length should be the same
                if (this.viewingStep && newVal && oldVal && newVal[this.viewingStep] && oldVal[this.viewingStep] && newVal[this.viewingStep].length === oldVal[this.viewingStep].length) {

                    // check if the new array has duplicate items. If yes, then it's something we might want to work on. (that means one goal has switched to the new rank, but the other one hasn't).
                    // Please note, duplicate rank value can also happen if multiple goals in the list are inactive (-1), so we will need to filter them out in the next step.
                    var valueArr = newVal[this.viewingStep].map(function (item) {
                        return item.rank
                    });
                    var hasDuplicate = valueArr.some(function (item, idx) {
                        return valueArr.indexOf(item) != idx
                    });

                    // find out what's the new rank and what's the old one
                    let newRank = null;
                    let oldRank = null;
                    for (var i = 0; i < newVal[this.viewingStep].length; i++) {
                        const newGoal = newVal[this.viewingStep][i]
                        const oldGoal = oldVal[this.viewingStep].find(element => element.id == newGoal.id);
                        if (newGoal.rank && oldGoal.rank && newGoal.rank !== -1 && oldGoal.rank !== -1 && newGoal.rank != oldGoal.rank) {
                            // if both newGoal and oldGoal are active, but their ranks are different
                            newRank = newGoal.rank;
                            oldRank = oldGoal.rank;
                        }
                    }

                    if (newRank && oldRank) {
                        const targetGoal = oldVal[this.viewingStep].find(element => element.rank === newRank);
                        if (targetGoal) {
                            this.formInputs.goalsAnalysis.goals[targetGoal.id].rank = oldRank; // complete the swap by assiging the other goal with the old rank

                            // // hacky thing to show a loader 'updating...' for 2 seconds to user indicating we are updating the order - in fact it happens instantly
                            // this.updating = true;
                            // setTimeout(() => this.updating = false, 1000);
                        }
                    }
                }


            },
            deep: true,
        },

        /**
         * Watcher for each step's goals analysis answers - if goal has already been generated and there are not empty answers which have changed between oldVal and newVal, then we consider this step's answers have changed.
         * @type {Object}
         */
        goalsAnalysisAnswers: {
            handler: function (newVal, oldVal) {
                if (newVal && oldVal) {
                    for (var i = 0; i < Object.keys(newVal).length; i++) {
                        const stepId = Object.keys(newVal)[i];
                        if (this.hasGoals(stepId)) {
                            for (var j = 0; j < Object.keys(newVal[stepId]).length; j++) {
                                const questionId = Object.keys(newVal[stepId])[j];
                                const newAnswer = newVal[stepId][questionId];
                                const oldAnswer = oldVal[stepId][questionId];
                                if (newAnswer && oldAnswer && newAnswer != oldAnswer) {
                                    this.goalsAnalysisAnswersHaveChanged[stepId] = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            },
            deep: true,
        },

        message: {
            handler: function (newVal, oldVal) {
                // console.log(newVal.error, oldVal.error);
                if(newVal.error 
                    // || newVal.info
                    ) {
                    Vue.nextTick(function() {
                        // console.log('here');
                        window.showPopup('notice')
                    });
                }
            },
            deep: true,
        },

        /**
         * Listen for any changes in each step. If changed, indicate that this step needs to be saved.
         * @type {Object}
         */
        formInputsByStep: {
            handler: function (newVal, oldVal) {
                if (newVal && oldVal) {
                    for (var i = 0; i < Object.keys(newVal).length; i++) {
                        const stepId = Object.keys(newVal)[i];

                        if (this.stepsThatNeedsSave.hasOwnProperty(stepId)) {
                            // if current step is already detected as unsaved, we don't need to check again.
                            if (this.stepsThatNeedsSave[stepId]) {
                                continue;
                            }

                            if (this.isGeneralQuestionStep(stepId)) {
                                // general questions
                                for (var j = 0; j < Object.keys(newVal[stepId]).length; j++) {
                                    const fieldHandle = Object.keys(newVal[stepId])[j];
                                    if (newVal[stepId][fieldHandle] != oldVal[stepId][fieldHandle]) {
                                        // console.log(newVal[stepId][fieldHandle], oldVal[stepId][fieldHandle]);
                                        // console.log(newVal, oldVal);
                                        this.stepsThatNeedsSave[stepId] = true;
                                        break;
                                    }
                                }
                            } else {
                                // optional goals
                                if(newVal[stepId].optionalGoal != oldVal[stepId].optionalGoal) {
                                    this.stepsThatNeedsSave[stepId] = true;
                                }
                                // goal analysis questions
                                for (var j = 0; j < Object.keys(newVal[stepId].answers).length; j++) {
                                    const questionId = Object.keys(newVal[stepId].answers)[j];
                                    if (newVal[stepId].answers[questionId] != oldVal[stepId].answers[questionId]) {
                                        this.stepsThatNeedsSave[stepId] = true;
                                        break;
                                    }
                                }
                                // goal ranks or notes
                                for (var j = 0; j < Object.keys(newVal[stepId].goals).length; j++) {
                                    const goalId = Object.keys(newVal[stepId].goals)[j];
                                    if (newVal[stepId].goals[goalId].rank != oldVal[stepId].goals[goalId].rank || newVal[stepId].goals[goalId].notes != oldVal[stepId].goals[goalId].notes) {
                                        this.stepsThatNeedsSave[stepId] = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            },
            deep: true,
        },

    },
    mounted() {
        axios
            .get('/actions/yodhub-module/planning/get-data-for-planning-tool')
            .then(response => {
                this.info = response.data;

                // check for the furthest viewable step: if a plan already exist, it should be the plan's currentStep value from the database. Otherwise, set it to 1
                if (this.info.plan && this.info.plan.currentStep) {
                    this.furthestViewableStep = this.info.plan.currentStep;
                } else {
                    this.furthestViewableStep = "1";
                }

                // the above step needs to happen before the below one, because isStepViewable needs to use furthestViewableStep
                // determine the current viewing step
                if (this.isStepViewable(window.loadStep)) {
                    // console.log('here');
                    // there's a chance that the landing step comes from the URL param (set in planning-vue.html). That should be the highest priority. But before we set it to be the viewingStep, we will need to validate whether that's viewable.
                    this.viewingStep = window.loadStep;
                } else if (this.info.plan && this.info.plan.currentStep) {
                    // console.log('there');
                    // second priority is to set the viewingStep to be the same as the plan's currentStep from db. This acts like "Continue filling the form", but the reality is that it will be very rare the code is executed inside this block, as it will only happen when user visits /planning/questionnaire directly without any URL params, which is very rare.
                    this.viewingStep = this.info.plan.currentStep;
                } 
                // else if (this.firstInvalidStep) {
                //     // most likely the user will come here when they hit 'results' on 'your plan', but they have invalid steps, so results is not viewable. - hmm this doesn't really work, because the firstInvalidStep is probably hasn't been calculated when coming to this condition check, so it skips this block and go to the next check (this.viewingStep = '1'). It's ok I guess, this is only nice-to-have
                //     this.viewingStep = this.firstInvalidStep;
                // } 
                else {
                    // otherwise, start with Step 1
                    this.viewingStep = "1";
                }

            })
            .catch(error => {
                // console.log(error)
                this.initErrored = true;
                this.message.error = "We're sorry, we're not able to retrieve the data at this moment, please check your internet and try refreshing your browser.";
            })
            .finally(() => this.initialising = false)
    },
});

/**
 * The popstate event is fired every time the current history entry changes. That happens when the user click on browser's Back/Forward buttons or programmatically by calling the history.back(), hitory.forward(), history.go() methods. See: https://zinoui.com/blog/single-page-apps-html5-pushstate, https://stackoverflow.com/questions/25806608/how-to-detect-browser-back-button-event-cross-browser
 *
 * We listen to this event, so we can update viewingStep to the state.step and it will update the DOM automatically. This solution is not perfect. It works okay, except for it does not align with our 'back' functionality in the app - it does not validate and attempt to save the current step (if logged in). This can be potentially problematic if users click the browser back button and then forgot to go back and save an unfinished step.
 *
 * One thought was to warn user before they hit the back button, or hit the browser close button that they could lose their work if they do so. But onbeforeunload event has been deprecated and not recommended to use anymore.
 * @param  {[type]} e) {               var state [description]
 * @return {[type]}    [description]
 */
$(window).on('popstate', function (e) {
    var state = e.originalEvent.state;
    if (state !== null) {
        // console.log(state);
        if (planningVue) {
            // planningVue.viewingStep = state.step;
        }
        // return false;
    }
});


function isInt(value) {
    return !isNaN(value) &&
        parseInt(Number(value)) == value &&
        !isNaN(parseInt(value, 10));
}

/**
 * This function is similar to Array.prototype.indexOf, however, that one use strict equality "===" to check for equality, which doesn't work for us in some cases when comparing integer with string type integers, so we wrote our custom function below to do the same thing but with not so strict equality checks "=="
 * @param  {mixed} item item to be searched in array
 * @param  {array} arr   array
 * @return index if found, or -1 if not found
 */
function indexOf(item, arr) {
    for (var i = 0; i < arr.length; i++) {
        if (item == arr[i]) {
            return i;
        }
    }
    return -1;
}

function updateUrlParams(viewingStep) {
    if (!$('html').hasClass('ieLessThan10')) { // otherwise we will hit js error and break other scripts

        var paramString = objToString({
            step: viewingStep,
        });

        var newUrl = window.location.origin + window.location.pathname + '?' + paramString;
        // we use pushState instead of replaceState is because we want users to be able to navigate through using the browser's back button. However, the results will not be saved if users do so.
        // UPDATED: we changed back to replaceState instead of pushState as it can cause potential issues. We rather let users go back to other pages instead of switching to a unfinished step which they can potentially save and overwrite their profile.
        // console.log(viewingStep);
        history.replaceState({step: viewingStep}, null, newUrl); // Doesn't work for IE 10 and less, might need to change this into window.location.hash
    }
}

function objToString(obj) {
    var str = '';
    for (var p in obj) {
        if (obj.hasOwnProperty(p) && obj[p]) {
            str += p + '=' + encodeURIComponent(obj[p]) + '&'; // need to encode the value, cuz users might put in white spaces, etc
        }
    }
    if (str.substr(str.length - 1) == '&') {
        str = str.substr(0, str.length - 1);
    }
    return str;
}

function scrollTo ($target, speed, callback) {
	if(!speed) speed = 300; // default
	$('html,body').animate({
        scrollTop: $target.offset().top
    }, speed, 'swing', callback ? callback : null);
}

/**
 * Deprecated - now do not handle login during the app - it's dangerous and buggy
 * @param  {[type]} e) {	e.preventDefault();	const postData [description]
 * @return {[type]}    [description]
 */
// $('#login-form').submit(function(e) {
// 	e.preventDefault();

// 	const postData = $(this).serialize();
// 	const action = $(this).find('[name="action"]').val();
// 	// console.log(action);

// 	if(action) {
// 		planningVue.login(postData, action);
// 	}


// });

$('#signup-form').submit(function (e) {
    e.preventDefault();

    const postData = $(this).serialize();
    const action = $(this).find('[name="action"]').val();
    // console.log(action);

    if (action) {
        planningVue.signup(postData, action);
    }
});

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}