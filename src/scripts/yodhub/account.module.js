window.showPopup = function(popupId) {
	const popup = document.getElementById(popupId);
	if(popup) {
		popup.classList.add('show');

		var $firstFormField = $(popup).find('.form-control').first();
		if($firstFormField && $firstFormField.length) {
			$firstFormField.focus();
		}
		
		$(document).keyup(function (event){
			if (event.key == "Escape") {
				window.hidePopup(popupId);				
			}
		});
	}
}

window.hidePopup = function(popupId) {
	const popup = document.getElementById(popupId);
	if(popup) {
		popup.classList.remove('show');
	}
}

// window.preventSubmit = function(e) {
// 	e.preventDefault();
// }

$(document).on('click', '.js-popup-close', function(e) {
	$(this).closest('.popup').removeClass('show');
})

$(document).on('click', '.popup-go-to', function(e) {
	const targetId = $(this).attr('data-goto');
	if(targetId) {
		$(this).closest('.popup').removeClass('show');
		showPopup(targetId);
	}
})

