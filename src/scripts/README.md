How to manage JS files (using Gulp)
---

* (Module files) Any files under /scripts folder with '.module.js' will not be directly compiled by Gulp (see gulpfile.js). Instead, they need to be imported by other js file if required, e.g. import './yumply/search.module.js';
* (Non-module files) Any files under /scripts folder excluding .module.js files will be directly compiled by gulp and exported into the public folder. That means, each one of them will have a file in public folder.
* main.js file is the master project JS file, which will be included in every template. So, use it to include the JS libraries required in every template. For other non-module files, they can be template specific, or feature specific. E.g. donation.js, news.js. They don't need to include all the basic libraries (like Bootstrap), since they are already included in main.js.