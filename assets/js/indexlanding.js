const apiToken = 'QpsADXskbNXo71ND-CUmljzH9R4ANnYu';
const apiUrl = '/api';
// The query to search for entries in Craft
const searchQuery = '{entries(section: ["events"], status: "live") {id,title,url}}';
// Configure the api endpoint
const configureApi = (url, token) => {
    return {
        baseURL: url,
        headers: {
            'Authorization': `Bearer ${token}`,
            'X-Requested-With': 'XMLHttpRequest'
        }
    };
};
const dataIndexCall = axios.create(configureApi(apiUrl, apiToken));

new Vue({
    delimiters: ['${', '}'],
    el: ".vue",
    data:{
        eventsIndex: [],
        commentsToShow: 2
    },
    mounted: function() {
        dataIndexCall.post('', {
            query: searchQuery
        }).then((result) => {
            this.eventsIndex = result.data.data.entries;
        }).catch((error) => {
            console.log(error);
        });
    }
});