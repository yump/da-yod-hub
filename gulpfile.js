"use strict";

const autoprefixer = require("gulp-autoprefixer");
const browserSync = require("browser-sync").create();
const noop = require("gulp-noop");
const changed = require("gulp-changed");
const sourcemaps = require("gulp-sourcemaps");
const gulp = require("gulp");
const options = require("gulp-options");
const sass = require("gulp-sass");
const runSequence = require("run-sequence");
const notify = require("gulp-notify");
const named = require("vinyl-named");
const plumber = require("gulp-plumber");
const webpack = require("webpack-stream");
// const imagemin = require('gulp-imagemin');
const MinifyPlugin = require("babel-minify-webpack-plugin");
const cleanCSS = require("gulp-clean-css");
const concat = require("gulp-concat");

// Local
const packageJson = require("./package.json");

//------------------------------------------------------------------------------
// Configuration.
//------------------------------------------------------------------------------
const env = options.get("env");
// Environment configuration.
const isProd = env === "production";
// const isProd = true;

// Directory configuration.
// Must have values, don't use leading or trailing slashes.
const dirs = {
  templates: "templates",
  entry: "src",
  output: "dist"
};

// Path configuration.
// Must have values, don't use leading or trailing slashes.
const paths = {
  // media: {
  //   src: [
  //   `${dirs.entry}/img/**/*.+(gif|jpg|jpeg|png|svg)`
  //   ],
  //   dest: `${dirs.output}/img`,
  // },
  // fonts: {
  //   src: `${dirs.entry}/fonts/**/*.+(eot|svg|ttf|woff)`,
  //   dest: `${dirs.output}/fonts`,
  // },
  styles: {
    src: `${dirs.entry}/styles/**/*.+(css|scss)`,
    dest: `${dirs.output}/styles`
  },
  scripts: {
    src: [
      `${dirs.entry}/scripts/**/*.js`,
      `!${dirs.entry}/scripts/**/*.module.js`
    ],
    dest: `${dirs.output}/scripts`
  },
  html: {
    src: [`${dirs.templates}/**/*.html`]
  }
};

// Plugin configurations.
// Use an empty object for empty configurations.
const pluginConfig = {
  autoprefixer: { browsers: ["last 2 versions"] },
  browserSync: {
    port: process.env.PORT || 3000,
    proxy: "yodhub.test"
  },
  cleanCSS: [
    { debug: true },
    ({ name, stats }) => {
      console.log(`Original size of ${name}: ${stats.originalSize} bytes`);
      console.log(`Minified size of ${name}: ${stats.minifiedSize} bytes`);
    }
  ],
  plumber: {
    errorHandler(...args) {
      notify
        .onError({
          title: "Compile Error",
          message: "<%= error.message %>",
          sound: false
        })
        .apply(this, args);
      this.emit("end");
    }
  },
  // imagemin: [
  //   imagemin.gifsicle({ interlaced: true, optimizationLevel: 3 }),
  //   imagemin.jpegtran({ progressive: true }),
  //   imagemin.optipng({ optimizationLevel: 7 }),
  //   // imagemin.svgo({
  //   //   plugins: [{ removeUselessDefs: false }, { cleanupIDs: false }, {convertPathData: false}],
  //   // }),
  // ],
  sass: {
    outputStyle: "expanded",
    includePaths: [
      "node_modules/bootstrap/scss",
      "node_modules/smartmenus/dist/addons/bootstrap-4",
      "node_modules/slick-carousel/slick"
    ]
  },
  sourcemaps: ".",
  webpack: {
    devtool: isProd ? "cheap-source-map" : "cheap-eval-source-map",
    mode: isProd ? "production" : "development",
    module: {
      rules: [
        {
          test: /\.js$/,
          loader: "babel-loader",
          options: { presets: ["env"] }
        }
      ]
    },
    plugins: isProd
      ? [new MinifyPlugin({ removeConsole: true }, { sourceMap: false })]
      : []
  }
};

//------------------------------------------------------------------------------
// Media.
//------------------------------------------------------------------------------

// gulp.task('media', () =>
//   gulp
//   // Input.
//     .src(paths.media.src)
//     // Report errors.
//     .pipe(plumber(pluginConfig.plumber))
//     // Production: Do nothing.
//     // Development: Pipe only changed files to the next process.
//     .pipe(isProd ? noop() : changed(paths.media.dest))
//     // // Production: Optimize.
//     // // Development: Do Nothing.
//     // .pipe(isProd ? imagemin(pluginConfig.imagemin) : noop())
//     // Production: Optimize.
//     // Development: Optimize.
//     .pipe(imagemin(pluginConfig.imagemin))
//     // Output.
//     .pipe(gulp.dest(paths.media.dest))
//     // Production: Do nothing.
//     // Development: Stream changes back to 'watch' tasks.
//     .pipe(isProd ? noop() : browserSync.stream())
// );

//------------------------------------------------------------------------------
// Fonts - Just copy to Web Folder
//------------------------------------------------------------------------------

// gulp.task('fonts', () =>
//   gulp
//   // Input.
//     .src(paths.fonts.src)
//     // Report errors.
//     .pipe(plumber(pluginConfig.plumber))
//     // Production: Do nothing.
//     // Development: Pipe only changed files to the next process.
//     .pipe(isProd ? noop() : changed(paths.fonts.dest))
//     // Output.
//     .pipe(gulp.dest(paths.fonts.dest))
//     // Production: Do nothing.
//     // Development: Stream changes back to 'watch' tasks.
//     .pipe(isProd ? noop() : browserSync.stream())
// );

//------------------------------------------------------------------------------
// HTML - Just reload the browser
//------------------------------------------------------------------------------
gulp.task("html", done => {
  browserSync.reload();
  done();
});

//------------------------------------------------------------------------------
// Styles.
//------------------------------------------------------------------------------

gulp.task("styles", () =>
  gulp
    // Input.
    .src(paths.styles.src)
    // Report errors.
    .pipe(plumber(pluginConfig.plumber))
    // Production: Do nothing.
    // Development: Pipe only changed files to the next process.
    .pipe(isProd ? noop() : changed(paths.styles.dest))
    // Start mapping original source
    .pipe(sourcemaps.init())
    // Convert to CSS.
    .pipe(sass(pluginConfig.sass))
    // Add browser compatibility.
    .pipe(autoprefixer(pluginConfig.autoprefixer))
    // Production: Minify.
    // Development: Do nothing.
    .pipe(isProd ? cleanCSS(...pluginConfig.cleanCSS) : noop())
    // Save mapping for easier debugging.
    .pipe(sourcemaps.write(pluginConfig.sourcemaps))
    // Output.
    .pipe(gulp.dest(paths.styles.dest))
    // Production: Do nothing.
    // Development: Stream changes back to 'watch' tasks.
    .pipe(isProd ? noop() : browserSync.stream())
);

//------------------------------------------------------------------------------
// Scripts.
//------------------------------------------------------------------------------

gulp.task("scripts", () =>
  gulp
    // Input.
    .src(paths.scripts.src)
    // Report errors.
    .pipe(plumber(pluginConfig.plumber))
    // Automatically pass named chunks to webpack.
    .pipe(named())
    // Bundle.
    .pipe(webpack(pluginConfig.webpack))
    // Output.
    .pipe(gulp.dest(paths.scripts.dest))
);

//------------------------------------------------------------------------------
// Serve.
//------------------------------------------------------------------------------

// Development.
// Starts the browserSync server.
gulp.task("serve", () => browserSync.init(pluginConfig.browserSync));

//------------------------------------------------------------------------------
// Watch.
//------------------------------------------------------------------------------

// Ensures the 'scripts' task is complete before reloading browsers.
gulp.task(
  "scripts:watch",
  gulp.series("scripts", done => {
    browserSync.reload();
    done();
  })
);

// Development.
// Watches files for changes.
gulp.task("watch", done => {
  gulp.watch(paths.styles.src, gulp.series("styles"));
  // gulp.watch(paths.media.src, ['media']);
  gulp.watch(paths.scripts.src[0], gulp.series("scripts:watch"));
  // gulp.watch(paths.fonts.src,['fonts']);
  gulp.watch(paths.html.src, gulp.series("html"));
  done();
});

//------------------------------------------------------------------------------
// Default.
//------------------------------------------------------------------------------
const compile = [
  "styles",
  "scripts",
  "html"
  // 'media',
  // 'fonts',
];

if(isProd) {
  gulp.task(
    "default",
    gulp.parallel(compile)
  );
} else {
  gulp.task(
    "default",
    gulp.series(compile, "watch", "serve")
  );
}