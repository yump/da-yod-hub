# Maintenance Log

## Maintenance report - Apr 2024
* Updated Craft and Plugins to the latest version as below:
    - craft 3.9.10 => 3.9.13
    - blitz 3.13.0 => 3.14.0
	- Neo 2.13.19 => 2.13.21
    - retour 3.2.12 => 3.2.14
    - seomatic 3.4.68 => 3.4.74
* Fix siteUrl deprecation error (fixed)
* CMS: uploading assets, entries, templates, SEO preview - all working fine
* Frontend: all pages ,  planning tools - all working fine
* cPanel:
	- File Usage: 97,543 / 300,000 (32.51%)
	- Disk Usage: 4.78 GB / 15 GB (31.85%)
	- Physical Memory Usage: 116.52 MB / 6 GB (1.9%)
	- MySQL® Disk Usage: 365.41 MB / 10.09 GB (3.37%)
	- error logs - all good
* Broken Links:
404 Page Not Found:
Link: https://www.dementia.org.au/helpline/webchat/
Link: https://www.dementia.org.au/helpline/webchat
Link From: Top Navigation => Chat

Link: https://www.dementia.org.au/services/counselling
Link From: https://yod.dementia.org.au/understanding-your-diagnosis/your-feelings
Link Text: Dementia Australia provides a free specialist counselling service for people with dementia, their family, friends and carers.

Documents 404 Not Found:
Document: Dementia Australia’s help sheet (PDF) on diagnosing dementia
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia02-DiagnosingDementia_english.pdf
Document From: 
	- https://yod.dementia.org.au/about-younger-onset-dementia/signs-and-symptoms
	- https://yod.dementia.org.au/about-younger-onset-dementia/health-professionals-you-may-meet
	- https://yod.dementia.org.au/about-younger-onset-dementia/health-professionals

Document: Read Dementia Australia’s help sheet (PDF) on memory changes.
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia12-MemoryChanges_english.pdf
Document From: https://yod.dementia.org.au/about-younger-onset-dementia/signs-and-symptoms

Document: Read Dementia Australia’s help sheet (PDF) on younger onset dementia
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-YoungerOnsetDementia01-Overview_english.pdf
Document From: https://yod.dementia.org.au/about-younger-onset-dementia/getting-a-diagnosis

Document: Read Dementia Australia’s help sheet (PDF) on talking about your diagnosis.
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-InformationForPeopleWithDementia08-TalkingAboutTheDiagnosis_english.pdf
Document From: 
	- https://yod.dementia.org.au/about-younger-onset-dementia/getting-a-diagnosis
	- https://yod.dementia.org.au/understanding-your-diagnosis/your-feelings

Document: Read Dementia Australia’s help sheet (PDF) on younger onset dementia
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia21-YoungerOnsetDementia_english.pdf
Document From: 
	- https://yod.dementia.org.au/about-younger-onset-dementia
	- https://yod.dementia.org.au/about-younger-onset-dementia/

Document: Read Dementia Australia’s help sheet (PDF) on diagnostic criteria for dementia.
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-DementiaQandA11-DiagnosticCriteriaForDementia_english.pdf
Document From: 
	- https://yod.dementia.org.au/about-younger-onset-dementia/health-professionals-you-may-meet
	- https://yod.dementia.org.au/about-younger-onset-dementia/health-professionals

Document: Read Dementia Australia’s help sheet (PDF) on Alzheimer’s disease.
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia13-AlzheimersDisease_english.pdf
Document From: https://yod.dementia.org.au/about-younger-onset-dementia/types-of-dementia-in-younger-people

Document: Read Dementia Australia’s help sheet (PDF) about how we can help support you.
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-InformationForPeopleWithDementia11-HowDementiaAustraliaCanHelp_english.pdf
Document From: 
	- https://yod.dementia.org.au/understanding-your-diagnosis
	- https://yod.dementia.org.au/about-younger-onset-dementia/health-profess

Document: Read Dementia Australia’s help sheet (PDF) on vascular dementia.
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia16-VascularDementia_english.pdf
Document From: https://yod.dementia.org.au/about-younger-onset-dementia/types-of-dementia-in-younger-people

Document: Read Dementia Australia’s help sheet (PDF) on frontotemporal dementia.
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia17-FrontotemporalDementia_english.pdf
Document From: https://yod.dementia.org.au/about-younger-onset-dementia/types-of-dementia-in-younger-people

Document: Read Dementia Australia’s help sheet (PDF) on alcohol-related dementia.
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia18-AlcoholRelatedDementiaAndWernickeKorsakoffSyndrome_english.pdf
Document From: https://yod.dementia.org.au/about-younger-onset-dementia/types-of-dementia-in-younger-people

Document: Read Dementia Australia’s help sheet (PDF) on Lewy body disease.
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia20-LewyBodyDisease_english.pdf
Document From: https://yod.dementia.org.au/about-younger-onset-dementia/types-of-dementia-in-younger-people

Document: Read Dementia Australia’s help sheet (PDF) on HIV-related dementia.
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia19-HIVAssociatedDementia_english.pdf
Document From: https://yod.dementia.org.au/about-younger-onset-dementia/types-of-dementia-in-younger-people

Document: Read Dementia Australia’s help sheet (PDF) on feelings and adjusting to change.
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-InformationForPeopleWithDementia06-FeelingsAndAdjustingToChange_english.pdf
Document From: https://yod.dementia.org.au/understanding-your-diagnosis/your-feelings

Document: Read Dementia Australia’s help sheet (PDF) on how to talk with your doctor.
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-InformationForPeopleWithDementia09-TalkingWithYourDoctor_english.pdf
Document From: https://yod.dementia.org.au/understanding-your-diagnosis/speaking-with-your-doctor

Document: Read Dementia Australia’s help sheet (PDF) on how to talk with your doctor.
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-InformationForPeopleWithDementia09-TalkingWithYourDoctor_english.pdf
Document From: https://yod.dementia.org.au/understanding-your-diagnosis/speaking-with-your-doctor

Document: 
	- Read Dementia Australia’s help sheet (PDF) for information about dementia for family and friends.
	- Read Dementia Australia’s help sheet (PDF) for information about dementia for young people.
	- Read Dementia Australia’s help sheet (PDF) for information about dementia for parents and grandparents.
Document From: https://yod.dementia.org.au/understanding-your-diagnosis/telling-family-and-friends

Document: 
	- Read Dementia Australia’s help sheet (PDF) on physical exercise and dementia.
	- Read Dementia Australia’s help sheet (PDF) on keeping involved and active
	- Read Dementia Australia’s help sheet (PDF) on health, wellbeing and lifestyle.
	- Read Dementia Australia’s guide (PDF) on walking safely with dementia.
Document From: 
	- https://www.dementia.org.au/files/helpsheets/Helpsheet-DementiaQandA08-PhysicalExercise_english.pdf
	- https://yod.dementia.org.au/living-well-with-dementia/social-connection

Document: 
	- Read Dementia Australia’s help sheet (PDF) on keeping involved and active.
Document From: 
	- https://yod.dementia.org.au/living-well-with-dementia/social-connection

404 Not found	
https://www.dementia.org.au/files/resources/Travelling-with-dementia.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/holidays-and-dementia

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-InformationForPeopleWithDementia03-LookingAfterYourself_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-CaringForSomeone08-Travelling_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/holidays-and-dementia

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-YoungerOnsetDementia07-HealthWellbeingAndLifestyle_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/physical-health

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-InformationForPeopleWithDementia05-LivingAlone_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia

404 Not found	
https://www.dementia.org.au/files/resources/2019-Walking-Safely-With-Dementia_A4_booklet_v8.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/physical-health

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-Environment02_SupportiveAids_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-YoungerOnsetDementia04-LanguageAndCommunication_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/social-connection

404 Not found	
https://www.dementia.org.au/files/resources/yod-and-ndis-booklet.pdf
Linked from: https://yod.dementia.org.au/understanding-your-diagnosis/ndis-support

404 Not found	
https://www.dementia.org.au/files/resources/2019-Lets-Talk-Booklet.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/social-connection

404 Not found	
https://www.dementia.org.au/files/resources/FamilyAndFriendsMatter_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/social-connection

404 Not found	
https://www.dementia.org.au/education/yod-and-the-ndis-by-joshua-moody
Linked from: https://yod.dementia.org.au/understanding-your-diagnosis/ndis-support

404 Not found	
https://livingwellwithdementia.org.au/wp-content/uploads/2015/08/Engage-Enable-Empower-A4-Book_web.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/social-connection

404 Not found	
https://www.dementia.org.au/files/resources/yod-and-ndis-checklist.pdf
Linked from: https://yod.dementia.org.au/understanding-your-diagnosis/ndis-support

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-InformationForPeopleWithDementia04-DrivingAndDementia_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/driving-and-dementia

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-CaringForSomeone07-Driving_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/driving-and-dementia

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-ChangedBehaviours03-DepressionAndDementia_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/depression-and-dementia

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-DementiaQandA15-DepressionAndDementia_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/depression-and-dementia

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-InformationForPeopleWithDementia02-EarlyPlanning_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/planning-for-the-future

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-YoungerOnsetDementia02-PlanningAhead-DecisionMakingCapacityAndTheLaw_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/planning-for-the-future

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-CaringForSomeone01-Communication_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/communication

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-CaringForSomeone02-TherapiesAndCommunicationApproaches_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/communication

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-Environment01_AdaptingYourHome_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/modifying-your-home

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-CaringForSomeone05-Activities_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/supporting-social-connection

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-CaringForSomeone04-CaringForSomeoneWhoLivesAlone_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/modifying-your-home

404 Not found	
https://www.dementia.org.au/information/resources/technology/dementia-friendly-home-app
Linked from: https://yod.dementia.org.au/living-well-for-carers/modifying-your-home

404 Not found	
https://www.dementia.org.au/files/resources/2019-Family-Friends-Community-booklet.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/supporting-social-connection

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-Environment03_HowToDesign_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/modifying-your-home

404 Not found	
https://www.dementia.org.au/education/list/carers
Linked from: https://yod.dementia.org.au/living-well-for-carers/modifying-your-home

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-CaringForSomeone03-SafetyIssues_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/addressing-challenges

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-ChangedBehaviours04-Wandering_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/addressing-challenges

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-DementiaQandA09-SaferWalkingForPeopleWithDementia_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/addressing-challenges

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-CaringForSomeone12-Eating_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/addressing-challenges

404 Not found	
http://www.dementia.org.au/helpline/webchat
Linked from: https://yod.dementia.org.au/dementia-australia-services-and-support-programs/helpline

404 Not found	
https://www.dementia.org.au/calendar
Linked from: https://yod.dementia.org.au/living-well-for-carers

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-LookingAfterFamiliesAndCarers01-TakingABreak_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-LookingAfterFamiliesAndCarers02-TakingCareOfYourself_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-LookingAfterFamiliesAndCarers03-Feelings_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-LookingAfterFamiliesAndCarers04-MenInASupportiveRole_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers

404 Not found	
https://www.dementia.org.au/event-product-types/living-with-dementia-program-for-clients
Linked from: https://yod.dementia.org.au/dementia-australia-services-and-support-programs/recently-diagnosed

404 Not found	
https://www.dementia.org.au/support/other-support-services
Linked from: https://yod.dementia.org.au/dementia-australia-services-and-support-programs/recently-diagnosed

404 Not found	
https://www.dementia.org.au/sites/default/files/helpsheets/Helpsheet-LookingAfterFamiliesAndCarers04-PartnersInACaringRole_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/carer-support

404 Not found	
https://www.dementia.org.au/information/for-health-professionals/clinical-resources
Linked from: https://yod.dementia.org.au/about-younger-onset-dementia/health-professionals/dementia-education-for-professionals

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia03-Diagnosis-InformingThePersonWithDementia_english.pdf
Linked from: https://yod.dementia.org.au/about-younger-onset-dementia/health-professionals/informing-the-person

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-InformationForPeopleWithDementia10-MakingEmploymentDecisions_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/planning-for-the-future/employment

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-ChangedBehaviours01-ChangedBehaviours_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/addressing-challenges/changed-behaviours

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-ChangedBehaviours06-AnxiousBehaviours_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/addressing-challenges/changed-behaviours

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-ChangedBehaviours07-AggressiveBehaviours_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/addressing-challenges/changed-behaviours

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-ChangedBehaviours08-AgitatedBehaviours_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/addressing-challenges/changed-behaviours

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-ChangedBehaviours09-HallucinationsAndFalseIdeas_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/addressing-challenges/changed-behaviours

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-ChangedBehaviours10-DisinhibitedBehaviours_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/addressing-challenges/changed-behaviours

404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-CaringForSomeone06-MakingTheMostOfRespiteCare_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/carer-support/respite-care



## Maintenance report - Jan 2024
* Updated Craft and Plugins to the latest version as below:
    - craft 3.9.4 => 3.9.10
    - blitz 3.12.10 => 3.13.0
    - retour 3.2.11 => 3.2.12
    - seomatic 3.4.61 => 3.4.68
* Fix siteUrl deprecation error (fixed)
* CMS: uploading assets, entries, templates, SEO preview - all working fine
* Frontend: all pages ,  planning tools - all working fine
* cPanel:
	- File Usage: 120,709 / 300,000 (40.24%)
	- Disk Usage: 5.25 GB / 15 GB (35%)
	- Physical Memory Usage: 81.68 MB / 6 GB (1.33%)
	- MySQL® Disk Usage: 347.86 MB / 10.09 GB (3.37%)
	- error logs - all good
* Broken Links:
404 Page Not Found:
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia20-LewyBodyDisease_english.pdf
Link from: https://yod.dementia.org.au/about-younger-onset-dementia/types-of-dementia-in-younger-people
Link text: Read Dementia Australia’s help sheet (PDF) on Lewy body disease

404 Page Not Found:
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-YoungerOnsetDementia07-HealthWellbeingAndLifestyle_english.pdf
Link from: 
	- https://yod.dementia.org.au/living-well-with-dementia/physical-health
	- https://yod.dementia.org.au/living-well-with-dementia/social-connection
	- https://yod.dementia.org.au/living-well-for-carers/addressing-challenges	
Link text: Read Dementia Australia’s help sheet (PDF) on health, wellbeing and lifestyle

404 Page Not Found:
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-LookingAfterFamiliesAndCarers04-MenInASupportiveRole_english.pdf
Link from: https://yod.dementia.org.au/living-well-for-carers	
Link text: Read Dementia Australia’s help sheet (PDF) about men in a supportive role

404 Page Not Found:
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia03-Diagnosis-InformingThePersonWithDementia_english.pdf
Link from: https://yod.dementia.org.au/about-younger-onset-dementia/health-professionals/informing-the-person
Link text: Read Dementia Australia’s help sheet (PDF) on informing a person with dementia

404 Page Not Found:
Link: https://www.dementia.org.au/files/helpsheets/Helpsheet-ChangedBehaviours08-AgitatedBehaviours_english.pdf
Link from: https://yod.dementia.org.au/living-well-for-carers/addressing-challenges/changed-behaviours
Link text: Read Dementia Australia’s help sheet (PDF) on agitated behaviours

Host Not Found:
Link: https://detectearly.org.au/
Link from: https://yod.dementia.org.au/about-younger-onset-dementia/health-professionals
Link text: Read more information on Dementia Australia’s Detect Early website

Host Not Found:
Link: https://disabilityadvocacyfinder.dss.gov.au/disability/ndap/
Link from: https://yod.dementia.org.au/living-well-with-dementia/planning-for-the-future/employment
Link text: anti-discrimination advocate

## Maintenance report - Oct 2023
* Updated Craft and Plugins to the latest version as below:
    - craft 3.8.15 => 3.9.4
    - imager-x v3.6.6 => v3.6.8
    - retour 3.2.10 => 3.2.11
    - seomatic 3.4.57 => 3.4.61
* CMS: uploading assets, entries, templates, SEO preview - all working fine
* Frontend: all pages ,  planning tools - all working fine
* cPanel:
	- File Usage: 123,410 / 300,000 (41.14%)
	- Disk Usage: 5.2 GB / 15 GB (34.69%)
	- Physical Memory Usage: 1.11 GB / 6 GB (18.47%)
	- MySQL® Disk Usage: 338.26 MB / 10.13 GB (3.26%)
	- error logs - all good

* Broken links:
Host not found	
https://detectearly.org.au/
Linked from: https://yod.dementia.org.au/about-younger-onset-dementia/health-professionals
404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia20-LewyBodyDisease_english.pdf
Linked from: https://yod.dementia.org.au/about-younger-onset-dementia/types-of-dementia-in-younger-people
404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-YoungerOnsetDementia07-HealthWellbeingAndLifestyle_english.pdf
Linked from: https://yod.dementia.org.au/living-well-with-dementia/physical-health and 2 more
404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-LookingAfterFamiliesAndCarers04-MenInASupportiveRole_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers
Host not found	
https://disabilityadvocacyfinder.dss.gov.au/disability/ndap/
Linked from: https://yod.dementia.org.au/living-well-with-dementia/planning-for-the-future/employment
404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-ChangedBehaviours08-AgitatedBehaviours_english.pdf
Linked from: https://yod.dementia.org.au/living-well-for-carers/addressing-challenges/changed-behaviours


# Maintenance Report: 29 March 2023 - Padam
● Updated Craft and Plugins to the latest version as below:
	○ Craft CMS: from “3.8.5” to “3.8.15”,
	○ Retour: from “3.2.9” to “3.2.10”,
	○ SEOmatic: from “3.4.52” to “3.4.57”,
	○ Blitz: from "3.12.8” to "3.12.10”,
	○ Neo: from “2.13.17” to "3.13.19”,
● CMS: uploading assets, saving entries, all entry sections, SEO preview all working fine
● Frontend: all pages and planning tools all looking fine
● Daily Backup - all good
● cPanel: error logs - No error found - all good
● cPanel: disk usage 5.04 GB / 15 GB (33.63%) - good
● cPanel: file usage 123,330 / 300,000 (41.11%) - good
● cPanel: MySQL® Disk Usage 319.9 MB / 10.27 GB (3.04%) - good
● Google search console - no erros
● Broken Links:
404 Not Found:
https://www.dementia.org.au/files/helpsheets/Helpsheet-AboutDementia20-LewyBodyDisease_english.pdf
Link Text: Read Dementia Australia’s help sheet (PDF) on Lewy body disease.
Link From: https://yod.dementia.org.au/about-younger-onset-dementia/types-of-dementia-in-younger-people

404 Not Found:
https://www.dementia.org.au/files/helpsheets/Helpsheet-LookingAfterFamiliesAndCarers04-MenInASupportiveRole_english.pdf
Link Text: Read Dementia Australia’s help sheet (PDF) about men in a supportive role
Link From: https://yod.dementia.org.au/living-well-for-carers

404 Not Found:
https://www.dementia.org.au/files/helpsheets/Helpsheet-ChangedBehaviours08-AgitatedBehaviours_english.pdf
Link From: https://yod.dementia.org.au/living-well-for-carers/addressing-challenges/changed-behaviours
Link Text: Read Dementia Australia’s help sheet (PDF) on agitated behaviours

# Maintenance Report: 29 March 2023 - Padam
● Updated Craft and Plugins to the latest version as below:
	○ Craft CMS: from “3.7.63” to “3.8.5”,
	○ Redactor: from “2.10.11” to “1.10.12”,
	○ Retour: from “3.2.7” to “3.2.9”,
	○ SEOmatic: from “3.4.45” to “3.4.52”,
	○ Blitz: from "3.12.7” to "3.12.8”,
	○ Imager-x: from “3.6.5” to “3.6.6”,
	○ Neo: from “2.13.15” to "3.13.16”,
● CMS: uploading assets, saving entries, all entry sections, SEO preview all working fine
● Frontend: all pages and planning tools all looking fine
● Daily Backup - all good
● cPanel: error logs - No error found - all good
● cPanel: disk usage 4.89 GB / 15 GB (32.61%) - good
● cPanel: file usage 121,601 / 300,000 (40.53%) - good
● cPanel: MySQL® Disk Usage 303.7 MB / 10.4 GB (2.85%) - good
● Google search console - no erros
● No broken link found

### maintenance report for 09-01-2023 - Padam

- Updated Craft and Plugins to the latest version as below:
    Craft CMS: from “3.7.56” to “3.7.63”,
    Redactor: from “2.10.10” to “1.10.11”,
    Retour: from “3.2.3” to “3.2.7”,
    SEOmatic: from "3.4.39” to “3.4.45”,
    Blitz: from "3.12.6” to "3.12.7”,
    Sprout-fields: from “3.8.5” to “3.8.6”

- CMS : uploading assets, saving entries, all entry sections, SEO preview - all working fine
- Frontend: all pages and planning tool - all looking fine
- cPanel: error logs - all good
- Daily Backup - all good
- cPanel: disk usage - 4.62 GB / 15 GB (30.8%) - good
- cPanel: file usage - 118,739 / 300,000 (39.58%) - good
- cPanel: MySQL® Disk Usage - 291.44 MB / 10.66 GB (2.67%)
- Google search console - no erros
- Broken links:
https://www.dementia.org.au/files/helpsheets/Helpsheet-LookingAfterFamiliesAndCarers04-MenInASupportiveRole_english.pdf
Linked from
https://yod.dementia.org.au/living-well-for-carers/carer-support
Link Text: "Read Dementia Australia’s help sheet (PDF) men in a supportive role."


### maintenance report for 18-10-2022

- Updated Craft and Plugins to the latest version as below:
    Craft CMS: from "3.7.48" to "3.7.56",
    Retour: from "3.1.73" to "3.2.3",
    SEOmatic: from "3.4.36" to "3.4.39",
    Blitz: from "3.12.5" to "3.12.6",
    Imager X: from "v3.6.2" to "v3.6.5",
    Neo: from "2.13.13" to "2.13.15",
    Table Maker: from "3.0.1" to "3.0.4"

- CMS : uploading assets, saving entries, all entry sections, SEO preview - all working fine
- Frontend: all pages and planning tool - all looking fine
- cPanel: error logs - all good
- Daily Backup - all good
- cPanel: disk usage - 4.38 GB / 15 GB (29.2%) - good  
- cPanel: file usage - 116,961 / 300,000 (38.99%) - good  
- cPanel: MySQL® Disk Usage - 287.89 MB / 10.9 GB (2.58%)
- Google search console - no erros
- Broken links:
    
404 Not found   
https://www.dementia.org.au/files/helpsheets/Helpsheet-LookingAfterFamiliesAndCarers04-MenInASupportiveRole_english.pdf
Linked from
https://yod.dementia.org.au/living-well-for-carers
https://yod.dementia.org.au/living-well-for-carers/carer-support

Link Text in both url : "Read Dementia Australia’s help sheet (PDF) men in a supportive role."

#### Maintenance report 19-07-2022

- Updated Craft and Plugins to the latest version as below:
    "craftcms/cms": from "3.7.38" to "3.7.48",
    "aelvan/craft-cp-element-count": from "^1.1.0" to "1.1.0",
    "craftcms/redactor": from "2.10.6" to "2.10.10",
    "ether/simplemap": from "3.8.2" to "3.9.4",
    "nystudio107/craft-retour": from "3.1.71" to "3.1.73",
    "nystudio107/craft-seomatic": from "3.4.29" to "3.4.36",
    "putyourlightson/craft-blitz": from "3.12.0" to "putyourlightson/craft-blitz": "3.12.5",
    "solspace/craft-freeform": from "3.13.7" to "solspace/craft-freeform": "3.13.14",
    "spacecatninja/imager-x": from "v3.5.6.1" to "spacecatninja/imager-x": "v3.6.2",
    "spicyweb/craft-neo": from "2.13.3" to "2.13.13",
    "supercool/tablemaker": from "2.0.1" to "verbb/tablemaker": "3.0.1",
	"verbb/field-manager": from "2.2.4" to "2.2.5"
- Updated staging site same as production  
- cPanel: error logs - all good
- cPanel: disk usage - 3.51 GB / 15 GB (23.42%) - good	
- cPanel: file usage - 62,690 / 300,000 (20.9%) - good	
- CMS : uploading assets, saving entries, all entry sections, SEO preview - all working fine
- Frontend: all pages and planning tool - all looking fine
- Google search console - no erros
- Broken links:
	
404 Not found	
https://www.dementia.org.au/files/helpsheets/Helpsheet-LookingAfterFamiliesAndCarers04-MenInASupportiveRole_english.pdf
Linked from
https://yod.dementia.org.au/living-well-for-carers
https://yod.dementia.org.au/living-well-for-carers/carer-support


#### Tasks Completed by Azadeh on 14 April 2022

- Updated Craft and Plugins to the latest version
- cPanel: logs - all good
- cPanel: disk usage - 3.56 GB / 15 GB (23.72%) - good	
- CMS : asset uploading, entry saving, all templates, SEO preview - all good
- Frontend: planning tool, all other pages - look good
- Google search console - no erros
- Updated staging site same as production  
- No broken links found


#### Tasks Completed by Azadeh on 10 Jan 2022

- Update Craft & Plugins to the latest version - Added imager x plugin in this maintenenace
- cPanel: logs - all good
- cPanel: Check disk usage and other usage status - 3.34 GB / 15 GB (22.29%) - good	
- Update staging site same as production  
- Tested CMS : asset uploading, entry saving, SEO preview - all good
- Frontend: planning tool, all templates look fine
- Google search console - all good
- No broken links
#### Tasks Completed by Azadeh on 06 August 2021

- Update staging site same as production  
- Update Craft & Plugins to the latest version - Added imager x plugin in this maintenenace
- Tested site + Planning tool all good
- Review Errors
	- No errors
	- Review the 'Not-handled' errors. Found the following popular hits. 
	  Please consider adding redirect rules via https://yod.dementia.org.au/admin/retour for the following hits.
	  - /understanding-your-diagnosis/post-diagnosis-questions/ - 8583 hits (this was reported in the last two maintenances as well)		
- No broken links
  - Check disk usage and other usage status - 3.17 GB / 15 GB (21.15%) - fine

#### Tasks Completed by Fei on 15 April 2021
- Backup all upload files and database
- Update staging site same as production  
- Update Craft & Plugins to the latest version
- Fix all dDeprecation issues
- Testing - all good
	- CMS Functionality
	- Content Blocks
	- Templates
	- Planning Tool
- Review Errors
	- Review logs - no concerning errors
	- Review the 'Not-handled' errors. Found the following popular hits. 
	  Please consider adding redirect rules via https://yod.dementia.org.au/admin/retour for the following hits.
	  - /understanding-your-diagnosis/post-diagnosis-questions/ - 8583 hits (this was reported in last maintenance)		
- Xmenu check broken link - all good
- Misc
  - Check disk usage and other usage status - 2.51 GB / 15 GB (16.74%) all good.

#### Tasks Completed by @Dean on 4 February 2021
- Update Craft & Plugins to the latest version
- Testing - all good
	- CMS Functionality
	- Content Blocks
	- Templates
	- Planning Tool
- Review Errors
	- Review logs - no concerning errors
	- Review the 'Not-handled' errors. Found the following popular hits. Please consider adding redirect rules via https://yod.dementia.org.au/admin/retour for the following hits.
/understanding-your-diagnosis/post-diagnosis-questions/	- 8578 hits (this was reported in last maintenance)

- Misc
Check disk usage and other usage status - all at a very low level, so all good.
Fixed a spelling error on the the Plan thank you page
- setup git deployment for both staging and production
- no time to look at the previous errors as ran into a few problems with updates + git deployment setup was a mission


### Notes for next maintenance - noted 30 Nov 2020 by Wei
- Two errors found in the log during the maintenance, double check in the next maintenance:
	- downloadable-pdf script: header already sent in web.log. I think this is a legacy error, but it keeps getting logged. The functionality is still working correctly, and it doesn't seem to do any harm. But maybe have a look in the future and see if it leaves any potential holes?
	- Retour report errors when visiting admin/dashboard: Module does not exist in the manifest: vendor.css - It only appears after the updates this maintenance. However, it seems like this is a plugin bug that the author needs to fix themselves?
- We still have two legacy issues of planning tool that we have no chance to fix yet:
	- Auto refresh CSRF token: https://app.asana.com/0/0/1195504271012697/f - this one is probably more important, but no one has reported an issue using the tool yet.


### 27 August 2020 - by Wei Lin
#### Tasks Completed
- Update Craft & Plugins to the latest version
- Testing - all good
	- CMS Functionality
	- Content Blocks
	- Templates
	- Planning Tool
- Review Errors
	- Review logs - no concerning errors
	- Review the 'Not-handled' errors. Found the following popular hits. Please consider adding redirect rules via https://yod.dementia.org.au/admin/retour for the following hits.

/understanding-your-diagnosis/post-diagnosis-questions/	- 6993 hits
/dementia-australia-services-and-support/	- 61 hits
/living-well-with-dementia/social-engagement/	- 60 hits
/understanding-your-diagnosis/what-is-ndis/	- 60 hits
/about-younger-onset-dementia/professionals-you-may-meet/	- 26 hits
/about-younger-onset-dementia/seeking-a-diagnosis/	- 21 hits
/dementia-australia-services-and-support-1/support-for-family-and-carers	- 21 hits

	- Review broken links: found a not-in-use broken image link in our code. This has now been fixed.
- Misc
	- Test SEO functionality - all good
	- Database has been backed up daily - yes
	- Check disk usage and other usage status - all at a very low level, so all good.

	